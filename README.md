# Wikipedia Citation Needed Experiment API

- [Wikipedia Citation Needed Experiment API](#wikipedia-citation-needed-experiment-api)
  - [Description](#description)
  - [How it Works](#how-it-works)
    - [Parties](#parties)
    - [Diagram](#diagram)
    - [Sequence](#sequence)
  - [Developing](#developing)
    - [Install Dependencies](#install-dependencies)
    - [Installing extra dependencies](#installing-extra-dependencies)
  - [Running](#running)
    - [Via Docker (Preferred)](#via-docker-preferred)
    - [Locally](#locally)
    - [Via ChatGPT](#via-chatgpt)
      - [Plugin Store](#plugin-store)
      - [Localhost](#localhost)
  - [Notes](#notes)

## Description

This API finds a quote from Wikipedia using [ChatGPT](https://chat.openai.com) and the [Wikipedia Search API](https://www.mediawiki.org/wiki/API:Search).

## How it Works

Below is a diagram of the interactions between the various parties as a user makes a query to our service.

### Parties

- **User**: The user of the ChatGPT interface
- **ChatGPT**: The ChatGPT interface
- **Wikipedia**: Wikipedia.org APIs
- **Citation Needed API**: This API

### Diagram

(Note, this diagram uses [Mermaid](https://mermaid.js.org/), a diagramming tool that enables you to create diagrams from text. These diagrams render within gitlab, but if you are viewing this in a different context, you may need to use a [Mermaid live editor](https://mermaid-js.github.io/mermaid-live-editor) to view the diagram, using the text below.)

```mermaid
    sequenceDiagram
      autonumber
      actor User
      participant Citation Needed API
      participant ChatGPT
      participant Wikipedia

      User-->>Citation Needed API: {{User Request}}<br/> "Verify: Alexandria's Genesis is a genetic mutation<br /> that gives its carrier purple eyes, shimmering pale<br /> skin and a lack of body hair"
      Citation Needed API->>ChatGPT: {{Keyword list request}} <br />{"instruction" : "Produce a list of keywords...", <br />"query": "Alexandria's Genesis is a genetic mutation..."}
      ChatGPT->>Citation Needed API: {{Keyword list response}} <br/> "Alexandria's Genesis, Genetic Mutation, Urban Legends"

      Citation Needed API->>Wikipedia: {{Article Request}} <br />GET /api.php?action=query&generator=search&gsrlimit=3&gsrsearch=Alexandrias_Genesis,...
      Wikipedia->>Citation Needed API: {{Article Response}} <br />{"pages": ["Mutation", "Genetic disorder", "List of urban legends"]}

      Citation Needed API->>Wikipedia: {{TOC Requests}} <br /> {"page_name": "Mutation"}
      Wikipedia->>Citation Needed API: {{TOC Response}} <br /> {"page": "Mutation", "toc": [...]}
      Citation Needed API->>Wikipedia: {{TOC Requests}} <br /> {"page_name": "Genetic disorder"}
      Wikipedia->>Citation Needed API: {{TOC Response}} <br /> {"page": "Genetic disorder", "toc": [...]}
      Citation Needed API->>Wikipedia: {{TOC Requests}} <br /> {"page_name": "List of urban legends"}
      Wikipedia->>Citation Needed API: {{TOC Response}} <br /> {"page": "List of urban legends", "toc": [...]}

      Citation Needed API->>ChatGPT: {{TOC Presentation}} <br />GET /openai/tbd <br/>{"instruction": "Select a list of paragraphs ...", <br />"context": "Alexandria's Genesis is a genetic mutation", <br />"query": [{"page": "Urban legend", "sections": [...]}, ...]}
      ChatGPT->>Citation Needed API: {{TOC Selection}} [{"page": "Urban legend", <br />"sections": [["Urban Legends": ["Origin and structure"], ...}]

      Citation Needed API->>Wikipedia: {{Section Lookup}} <br />{"page_name": "Mutation", "section": "Myths"}
      Wikipedia->>Citation Needed API: {{Section Response}} <br />{"wikitext": "..."}
      Citation Needed API->>Wikipedia: {{Section Lookup}} <br />{"page_name": "Genetic disorder", "section": "Legends"}
      Wikipedia->>Citation Needed API: {{Section Response}} <br />{"wikitext": "..."}
      Citation Needed API->>Wikipedia: {{Section Lookup}} <br />{"page_name": "List of urban legends", "section": "Alexandria's Genesis"}
      Wikipedia->>Citation Needed API: {{Section Response}} <br />{"wikitext": "..."}

      Citation Needed API->>ChatGPT: {{Verification Request}}<br />{"instruction" : "Select a quote...", <br />"query": {["Mutation", "Myths", "..."], ...}", <br/>"context": "Alexandria's Genesis is a genetic mutation..."}
      ChatGPT ->> Citation Needed API: {{Verification Response}}<br />["List of urban legends", "Alexandria's Genesis", "..."]

      Citation Needed API-->>User: {article: {"url": "...", "title": "...", "section": "...", ...},<br />result: one of correct, incorrect, partially_correct<br />no_result, quote_found, unknown,<br />explanation: ChatGPT generated text}


```

### Sequence

0. **Sentence selection**: In a corresponding UI (such as the Citation Needed Chrome Extension), the user selects a sentence or a short paragraph to find citations for.
1. **User Request**: This API receives the user request.
2. **Keyword extraction**: The API forwards the selected text and an instruction what to do with it to ChatGPT.
3. **Keyword lookup**: The returned keywords are forwarded to Wikipedia to retrieve matching Wikipedia pages and their table of content.
4. **Section selection**: The pages titles and their tables of content are sent to ChatGPT with an instruction to select up to three of these sections for creating a citation.
5. **Section lookup**: The selected sections are retrieved from Wikipedia and parsed to plain text.
6. **Citation extraction**: All selected section content is forwarded to ChatGPT with instructions to select a text segment that best matches the initial query.
7. **Citation matching and normalization**: The returned citation is compared with the texts from Wikipedia, and the best match is returned verbatim.

## Developing

### Install Dependencies

This app uses [Poetry](https://python-poetry.org/) to manage dependencies and coordinate virtual environments. To install Poetry, see [here](https://python-poetry.org/docs/#installation).

Once installed, you can install essential dependencies with:

```bash
poetry install --all-extras
```

### Installing extra dependencies

Note the `--all-extras` argument above. [Package extras](https://python-poetry.org/docs/pyproject/#extras) are a way to separate out functionality to keep the core installable package small. If you simply run `poetry install` you will not get the `script` or `notebooks`-required packages (see `pypackage.toml` for the `extras` section which defines these).

Either run `poetry install --all-extras` or `poetry install -E notebooks` / `poetry install -E scripts` to get the necessary dependencies after having run vanilla `poetry install`.

In order to add additional use-case specific dependencies, run `poetry add PACKAGE` to get an entry within the `pyproject.toml` file, then modify the entry under `tool.poetry.dependencies` to read `{"version" = "^X.Y.Z", optional = true }` (where `X.Y.Z` is the original version number), then add the dependency to the appropriate list under `[tool.poetry.extras]`.

## Running

### Locally

Without setting up Docker, you can run the following to run the app locally:

```bash
poetry run api
```

For debugging purposes (TODO: enable debugging Docker) - you can run the app with the `FastAPI: Debug Server` configuration in VSCode. You shouldn't need the `FastAPI: Prod Server`, but it is provided for reference to allow changing the logging configuration.

### Extracting requirements.txt

For deployment, it might be neccessary to create a requirements.txt file. This can be exported from
poetry:

```bash
poetry export --without-hashes --format=requirements.txt > requirements.txt
```

## Running on Toolforge

To deploy the repo on Toolforge:

1. Connect to Toolforge via SSH
2. Switch to correct user
   `become citation-needed-api`
3. Rebuild using the gitlab repo
   `toolforge build start https://gitlab.wikimedia.org/repos/future-audiences/citation-needed-api`
4. start/restart the webservice
   `toolforge webservice buildservice restart --mount=all`

## Notes

Below are a collection of useful notes about some of the particulars around how the plugin works:

- [Languages](./docs/languages.md) - A description of how we handle languages in the plugin
- [User Testing](./docs/user_testing.md) - Instructions for users on how to test the API
- [Logs](./docs/logs.md) - Information about querying user logs within AWS
- [Notebooks](./docs/notebooks.md) - Information about how to setup and use Jupyter notebooks
- [Feature Flagging](./docs/feature_flagging.md) - Information about how we use feature flagging in the project

## Export logs from toolforge

Run the following as citation-needed-api on the toolforge SSH prompt:

´´´bash
toolforge jobs run --image tool-citation-needed-api/tool-citation-needed-api:latest --command "export-logs" --wait --mount=all --no-filelog export-logs-once
´´´

## Export and delete logs from toolforge

´´´bash
toolforge jobs run --image tool-citation-needed-api/tool-citation-needed-api:latest --command "export-and-delete-logs" --mount=all --no-filelog export-and-delete-logs-once
´´´

## Set up scheduled daily log export

List running jobs:

```bash
toolforge jobs list
```

Start new scheduled job:

´´´bash
toolforge jobs run --image tool-citation-needed-api/tool-citation-needed-api:latest --command "export-and-delete-logs" --mount=all --no-filelog export-and-delete-logs-daily --schedule "@daily"
´´´
