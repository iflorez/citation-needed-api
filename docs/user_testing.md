# User Testing

## Context

We are initially opening the plugin up for limited alpha testing to a hand-picked group of individuals. We will be using this group to help us identify bugs and usability issues, and to help us prioritize our backlog of features. We will also be using the feedback from this group to help us determine when we are ready to open the plugin up to a wider audience.

## Pre-requisites

Every member of the limited alpha group has been sent the following four things:

1. A link to the plugin
2. A link to a form to report issues
3. A link to these instructions.

You will need to have this information handy to work through the below instructions.

## Limitations

At the time of writing this (Mid-May 2023), the following limitations hold for all ChatGPT users:

1. The use of ChatGPT plugins generally is limited to a subset of (paid) ChatGPT+ Users, or members of an organization that has plugins enabled.
2. Even for users who have access to ChatGPT plugins, the Wikipedia plugin is not listed in the Plugin Store.

At this time, this means that it can only be installed via a direct link, and only by select users. This link is only available to those who have been invited to participate in the alpha. If you have not been invited to participate in the alpha, you will not be able to install the plugin at this time.

We have plans to expand the audience we are testing with in the near future, please watch this space if you are interested!

## Instructions

1. Go to <https://chat.openai.com>. You should see a login screen like the one below. Click "Log In".
![Login Screen](images/user_testing/0-Login1.png)
2. Select "Continue with Google" to sign in via your WMF-affiliated Google workspace account. ![Login With Google](images/user_testing/0-Login2.png)
3. You will be redirected to a Google login screen. Enter your WMF-affiliated Google workspace email address and click "Next". ![Google Login](images/user_testing/0-Login3.png)
4. Go to <https://chat.openai.com/?model=gpt-4-plugins>
5. Click the "Plugins" drop-down menu
6. This will initially show "no plugins enabled". Click "Plugin Store" to open the Plugin Store. ![Plugin Store Link](images/user_testing/3-PluginStoreLink.png)
7. The Plugin Store will open in a modal. From here you can browse and select plugins. **Note that as written above, this is a closed initial alpha, so the Wikipedia plugin is not listed in the plugin store**. From here, click "Develop your own plugin". ![Plugin Store](images/user_testing/4-PluginStore.png)
8. Enter the testing URL that was given to you in the field provided and click "Find manifest file". ![URL Entry](images/user_testing/5-URLEntry.png)
9. ChatGPT will now verify that the plugin's metadata is correct. You should see two green checkmarks. Hit "Next". ![Plugin Verification](images/user_testing/6-PluginVerification.png)
10. You will be reminded of the limitation on the number of developers allowed to use a plugin. (Note that we are keeping track of this number internally and you do not have to worry about this). Click "Install for Me". ![Limitation Reminder](images/user_testing/7-LimitationReminder.png)
11. ChatGPT will remind you that this is an unverified plugin. Click "Continue". ![Unverified Plugin Warning](images/user_testing/8-UnverifiedPluginWarning.png)
12. Click "Install plugin" to continue. ![Install Plugin Button](images/user_testing/9-InstallPluginButton.png)
13. You will see the dropdown expanded and there will be a blue checkmark indicating that the plugin has been installed. You are now ready to start using the plugin! ![Enabled Plugin Indication](images/user_testing/10-EnabledPluginIndication.png)
14. With the plugin installed and enabled, there is one last hurdle: ChatGPT needs to determine that the particular query you send should be be routed to the plugin. **Note: this is not guaranteed! (See more below)** It is possible that the plugin will either not trigger, or that antoher plugin will trigger. In fact, you can also manually enable up to 2 other plugins, and they may all "compete" to fire. The below image shows that it looks like when it does fire, here being asked the question "Who is Jimmy Wales". Note the indicator just after the prompt text which says "Used Wikipedia (Unverified)". This is proof that our plugin has indeed been triggered. ![Example Use](images/user_testing/11-ExampleUse.png)
15. When the plugin has been triggered, it is possible to see more information by clicking "Used Wikipedia (Unverified)". From here, you can see a summary of 1) what ChatGPT believed your query to be , and 2) the exact response returned back from the plugin that was used to inform the response. ![Example Use Expanded](images/user_testing/12-ExampleUseExpanded.png)

## FAQ

1. What is the link to the plugin?
As we are currently in a closed alpha, we are not making the link to the plugin available at this time to the public. If you are on the invite list, you should have been sent this link.

1. When will this be available to the public?
We are currently in a closed and selective alpha, and are planning to expand the audience we are testing with in the future. Please watch this space if you are interested!

1. The plugin is not triggering, what should I do?
ChatGPT will not trigger the plugin for every query. It is possible that it will not trigger at all, or that another plugin will trigger instead. If you are having trouble getting the plugin to trigger, please try prefacing the query with "Search Wikipedia" or "ask wikipedia", which has a higher rate of success.

1. Something is wrong about the output, what do I do?
Please fill out [this form](https://docs.google.com/forms/d/1c_KeeYzC4IT3s5akSePcDciQJ1y2Nwr39kaLN980kLA/edit) . We will review this and get back with you.

1. How many times can I fill out this form?
As many times as you like! we will collect all responses together and review them.

1. I have a question that is not answered here, what do I do?
Please reach out to us via [the form](https://docs.google.com/forms/d/1c_KeeYzC4IT3s5akSePcDciQJ1y2Nwr39kaLN980kLA/edit) or email <futureaudiences@wikimedia.org>. We will review this and get back with you.
