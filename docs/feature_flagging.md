# Feature Flagging

## Introduction

This app uses our Gitlab Enterprise instance feature flags feature. See more about how this works [here](https://docs.gitlab.com/ee/operations/feature_flags.html).

Under the hood, this uses a subset [Unleash](https://github.com/Unleash/unleash) framework, and integrates it with the gitlab UI.

Due to our current CI/CD setup, these feature flags are only visible to administrators who have access to [Wikimedia Enterprise](https://gitlab.enterprise.wikimedia.com/), but the below will explain how this works for those who have this access.

## Current Flags

As of July 7, 2023, we have the following flags:

1. `accepting_new_users` - not currently in use, but if enabled, and we have the oauth flow active, sends users to a holding page instead of directing them to complete the oauth flow.
2. `google_search_is_enabled` - whether to enable performing searches using the Google Custom Search API
3. `on_blocklist` - a means of consulting a blocklist of users based on ephemeral ID -- see more below about User Lists.

## Viewing flags

To view flags, you can go to `Deployments -> Feature Flags` in the Wikimedia Enterprise UI. It looks like the below:

![View Flags](feature_flagging/0-view-flags.png)

## Editing flags

To edit flags (if you have permissions), click the pencil icon. Note you can edit which environments the flag is active on, and what the size of the affected userbase is (which can enable interesting features like percentage rollout).

![Editing Flags](feature_flagging/1-editing-flags.png)

### Boolean flags

The default for new flags is a boolean-type flag, which is either true or false. See above for an example of the interface for setting these types of flags.

### User List flags

Another type of flag is the User List flag, which applies a flag for a given subset of identified users. Clients pass the flag name and the userId, and the service will determine whether the flag is enabled for that user.

In our app, we don't have users beyond the "ephemeral id" that chatGPT sends us (an anonymized identifier that expires every 24 hours), but this is what we will be using for this purpose.

This is what the editing interface looks like for this kind of property:

![Edit User List Property](feature_flagging/2-edit-user-list-property.png)

Note that the type is "User List", and we have selected the list "Blocklist" as our list.

To see the list, go up one level to the view flags screen, and click "view user lists".

This is what the user list screen looks like:

![View User List](feature_flagging/3-view-user-list.png)

If you click on the pencil, you can view user list membership:

![View User List Membership](feature_flagging/4-view-user-list-membership.png)

From here you can delete existing users. You can also add a list of new users using the "Add users" button:

![Add user to user list](feature_flagging/5-add-user-to-user-list.png)

## Feature Flag Client

This app uses the [Unleash client SDK for Python](https://github.com/Unleash/unleash-client-python) to connect with and query feature flags.

See the library documentation above for more information.

See more info about our usage in [feature_flagging_esrvice.py](../app/services/feature_flagging_service.py).

### Cache

The default behavior of this library is to poll every 15 seconds for a change in flag status. We have changed this to 900 seconds / 15 minutes to minimize server load. Between refresh windows, the app relies on a cached value. Consequently, changes are not immediate.
