# Logs

- [Logs](#logs)
  - [Writing Logs](#writing-logs)
    - [Application Logging](#application-logging)
  - [Reading Logs](#reading-logs)
    - [User logs](#user-logs)
      - [Pre-Requisites](#pre-requisites)
    - [Installing Dependencies](#installing-dependencies)
    - [Running](#running)
    - [Debugging](#debugging)

## Writing Logs

### Application Logging

When writing logs in this app, please use the `structlog` module instead of `logging`. The API of these two modules is the same, but the former provides allows you to make logs more easily machine readable. To obtain a logger in a new file, run :

```python
from structlog import getLogger
logger = getLogger(__name__)
```

## Reading Logs

### User logs

There are several ways to get relevant user logs from CloudWatch on AWS. For bulk export, the easiest is Cloudwatch Insights, which we call out to via the `get_logs.py` script.

#### Pre-Requisites

To run the script, you need to create an IAM user that has the appropriate permissions, download the AWS command line interface, and configure it to use that user's credentials. To do that:

1. Create / access an AWS IAM user with an associated policy that has the appropriate permissions, specifically: `CloudWatchLogsReadOnlyAccess`, `StartQuery`, `GetQueryResults`, `iam:CreateAccessKey`. Work with SRE to get this.
1. Obtain the [aws CLI utility](https://github.com/aws/aws-cli) to run the below AWS commands.
1. Obtain AWS access key and secret (more info [here](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html))
1. Run `aws configure` and pass in those values, along with the value of the region of your aws instance.
1. Run the below `aws` commands

### Installing Dependencies

Scripts have a different set of dependencies than the app itself. They are specified as a poetry [package extra](https://python-poetry.org/docs/pyproject/#extras) named `scripts`. To get the necessart dependencies, run:

```bash
poetry install --all-extras
```

to get all extras, or just:

```bash
poetry install -E scripts
```

to get those necessary for scripts

### Running

To run the script, you can do:

```bash
poetry run get_logs --help 
```

to see all the arguments. An example invocation is:

```bash
poetry run get_logs --start-date 2023-05-19 --end-date 2023-05-30
```

This will default to prod logs, output as a csv to `results.csv`.

### Debugging

You can use the `Script: Get Logs` target in VSCode to run the script with some default arguments for debugging purposes.
