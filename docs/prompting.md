# Prompting ChatGPT

## Context

In a traditional API interface, code alone determines the contract between provider and consumer. This contract is often formalized in an [OpenAPI specification](https://en.wikipedia.org/wiki/OpenAPI_Specification) or similar document, but it is up to a human to determine when to call the right endpoints with the right parameters.

In the case of ChatGPT, however, the contract between provider and consumer is not entirely driven by code. In addition, it makes use of text-based descriptions of interfaces and parameters as well. Essentially then, our descriptions of our API's behavior and parameters become a variation of [prompt engineering](https://en.wikipedia.org/wiki/Prompt_engineering).

We have found in practice that small changes to values in manifest files and in code comments can have significant effects on how ChatGPT interacts with our plugin.

All of this is to say, ChatGPT is a service that is driven by processes that are not entirely under our control, but we have the ability to influence these processes by making small, text-based changes to parameters and descriptions.

### What we can influence

As a result, there is significant latitude to influence the following properties simply by means of changing text-based descriptions, often with no code changes required:

1. When ChatGPT decides to call our plugin (and when it does not)
   1. Note that often in practice ChatGPT is non-deterministic - it may call our plugin one time, and not call it another time, even if the input is the same.
2. What parameters ChatGPT passes to our plugin
   1. ChatGPT has the interesting property that you can simply declare that you need a certain piece of information with a certain name, type, and description, and it will assemble that information from the user's text for you and pass it to the plugin.
3. When it decides to pass these parameters
   1. You may notice that sometimes ChatGPT calls the plugin twice. This is often due to the plugin replying to an initial request with an error that indicates that a given parameter was not present. In this event, ChatGPT calls our plugin again and attempts to supply the parameter that was called out as missing.

This document is an attempt to document the various ways in which ChatGPT prompts our plugin, and how we can influence this behavior.

## Plugin Manifest

This is described in their documentation [here](https://platform.openai.com/docs/plugins/getting-started/plugin-manifest) . This file contains descriptions for both humans and machines about what the plugin as a whole does. If we need to update this file, we need to re-submit our plugin to the plugin store. Our live plugin manifest is [here](https://chatgptplugin.enterprise.wikimedia.com/.well-known/ai-plugin.json). The specific relevant properties and where they're set:
a. [MANIFEST_DESCRIPTION_FOR_HUMAN](https://gitlab.wikimedia.org/repos/machine-learning/chatgpt-plugin/-/blob/dev/app/constants.py#L39) - the human-readable description of our plugin, provided as the property `description_for_human` in the manifest. Our current value for this is:

> Ask questions about general knowledge, current events, and breaking news, and get up-to-date answers from Wikipedia.

Though nominally for human consumption, I would not be surprised if ChatGPT itself uses this information for routing purposes as well.
b. [MANIFEST_DESCRIPTION_FOR_MODEL](https://gitlab.wikimedia.org/repos/machine-learning/chatgpt-plugin/-/blob/dev/app/constants.py#L40)  - the GPT-readable version of what the plugin does, provided by the property `description_for_model` in the manifest. Our current version of this:

> Search Wikipedia to answer users' general knowledge, current events, and breaking news questions. Use it whenever a user asks for information that might be found on Wikipedia, passing in the user's exact text and performing a search using relevant parsed information as the query.

## OpenAPI Spec

More info about these [here](https://en.wikipedia.org/wiki/OpenAPI_Specification) in a general sense, and about ChatGPT's use of this [here](https://platform.openai.com/docs/plugins/getting-started/openapi-definition).  This describes what URL endpoints exist and specifics as to the parameters these endpoints take. You can change the descriptions here without needing to re-submit the app to the store. In our plugin, these values are populated by means of code comments, and this file is auto-generated. Our live spec is [here](https://chatgptplugin.enterprise.wikimedia.com/openapi.json). Specifically relevant sections of this:

a. [ENDPOINT_SUMMARY](https://gitlab.wikimedia.org/repos/machine-learning/chatgpt-plugin/-/blob/dev/app/routes/search_wikipedia.py#L21) - the code comment associated with the `/search_wikipedia` endpoint (currently our only endpoint) is probably the most important - our current iteration of this is:

> Takes a question from the user, plus the original text that the user sent and the inferred language that it was sent in, performs a search against Wikipedia, and returns back text from four relevant articles. ChatGPT should answer the provided question using the contents from these articles.

b. [SearchWikipediaParams](https://gitlab.wikimedia.org/repos/machine-learning/chatgpt-plugin/-/blob/dev/app/models/parameters.py#L70) - there are various comments in this file describing the parameters that consumers of our API (here, ChatGPT itself) can pass to us - the descriptions of what these parameters are and how to use them determines what information GPT passes to us and how. As an example, our description of the parameter `query_language`  is:

> The language of the query.  Defaults to English.

## Extra Information to Assistant

The `extra_information_to_assistant parameter` is part of the dictionary we pass back from the plugin. We ultimately just made this up, and it's not formally documented anywhere, but it seems to be a pattern other plugins have adopted and it seems to influence results. You can see this statement every time you expand the triangle next to `Used Wikipedia` in the ChatGPT interface. This is made of the following:

a. [PROMPT_SENTENCES](https://gitlab.wikimedia.org/repos/machine-learning/chatgpt-plugin/-/blob/dev/app/prompt_constants.py#L15) - `extra_information_to_assistant is assembled from a series of statements in this file, which are joined together at the end into a single statement. Our full current statement is:

> In ALL responses, If Assistant includes information from sources other than Wikipedia, Assistant MUST indicate which information is not from Wikipedia. In ALL responses, Assistant MUST always link to the Wikipedia articles used. In ALL responses, Assistant MUST finish by saying this exact text: 'This answer is based on content from [Wikipedia](https://www.wikipedia.org/), a free encyclopedia made by volunteers and available under a [Creative Commons Attribution-ShareAlike License](https://creativecommons.org/licenses/by-sa/4.0/). Please note that, as a large language model, I may not have summarized Wikipedia accurately.'. In ALL responses, the language of the assistant response MUST match the language of the articles in the returned array. Assistant MUST NOT translate the returned article text or summary into another language. Assistant MUST preserve the returned parameters from article_url of the returned articles when citing article contents.. If there are no results in the requested language, Assistant MUST respond with the text 'There are no Wikipedia articles available in the language requested' and MUST NOT attempt a subsequent fallback search in English
