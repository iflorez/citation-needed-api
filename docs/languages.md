# Languages

## Wikipedia Context

As of the time of writing this, there are [321](https://meta.wikimedia.org/wiki/List_of_Wikipedias) different active language versions of Wikipedia.

These different language versions of Wikipedia are in effect separate websites, with their own content, and their own community of editors. Different language versions of articles on Wikipedia are not simply translations of one another - they are different lenses on the same content, incorporating different cultural backgrounds and assumptions.

It is thus important that users of our plugin get back information that is relevant to the version of Wikipedia that corresponds to the language they are performing searches in.

## LLM Context

Large Language Models, such as those that power ChatGPT, were trained on a large, multilingual corpus of data. This corpus allows one to perform searches in any language, and get back results in any language, having been essentially "translated" by means of the model.

## The Problem

Because of the advanced capabilities of LLMs, we have found that if using our plugin to search in language X returns results from the language Y version of Wikipedia, the GPT model will translate the content into the language X that the user is performing the search in.

This is not what we want, however. Because of the importance of the cultural context noted above in the Wikipedia artilces of a given language, simply translating content from one language to another is not sufficient. We need to ensure that we are only returning results from the language version of Wikipedia that corresponds to the language that the user is performing the search in, so as to capture the unique curltural context of that language.

## The Solution

To accomplish this filtration, we have four levels of filtration:

1. We ensure we have captured what ChatGPT believes the user's language to be. This is done by means of the `query_language` field in the query object that is passed to our plugin.
2. We ensure that as we search for results within wikipedia, we only return results from the language version of Wikipedia that corresponds to the language that the user is performing the search in. This is done by means of the `lr` (language restrict) field in the query object that is passed to the Google Custom Search API.
3. We ensure that we only return results from the language version of Wikipedia that corresponds to the language that the user is performing the search in. This is done by means of comparing against the subdomain of the site
4. We attempt to ensure that the output language is the specified language. We do this by means of 1) The `output_language` field in the query object that is passed to our plugin, and 2) Explicit instructions in our prompt to the GPT model to use the specified language and return no results if none were found in the language of search.

## The Caveats

Despite the above measures, it is not always possible to completely avoid LLM-based translation of returned article content from another language version of Wikipedia. We are continuing to work through this issue.
