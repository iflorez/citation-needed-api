""" Main entrypoint for the application. """

# grequests must be imported before any modules that might import requests
import grequests
import uvicorn
from fastapi import FastAPI

from app.config.server_config import ServerConfigurer

# Logs must be configured before initializing the app
configurer = ServerConfigurer()
configurer.configure_logging()

app = FastAPI()
configurer.configure_app(app)


def main():
    """Launched with `poetry run api` only"""
    uvicorn.run("app.main:app", host="0.0.0.0", port=8000, reload=False, workers=1)


if __name__ == "__main__":
    main()
