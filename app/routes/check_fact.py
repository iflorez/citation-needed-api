# """ This module contains the route for searching Wikipedia for the given query and returning the results. """

# from typing import Any
# from fastapi import APIRouter, HTTPException
# from structlog import getLogger
# from openai import OpenAI
# from time import sleep
# from fastapi.responses import JSONResponse

# from app import constants
# from app.models.parameters import (ChatGPTToolCallResult, FactCheckResult, FactCheckerArticleResponse, FactCheckerRequestInfo, FactCheckerResponse, SearchWikipediaParams)
# from app.services.feature_flagging_service import get_feature_flagging_service
# from app.utils.settings_types import SystemEnum
# import re
# import requests
# from requests import Response
# import urllib.parse
# import json

# router = APIRouter()

# logger = getLogger(__name__)


# @router.get("/check_fact",
#             response_model=FactCheckerResponse,
#             summary="""Takes a statement from the user, fact checks it, and returns a matching wikipedia article.""",
#             )
# async def check_fact(selection: str) -> FactCheckerResponse | HTTPException:
#     """Takes a statement from the user, fact checks it, and returns a matching wikipedia article"""

#     full_request_info = FactCheckerRequestInfo(selection=selection)

#     logger.info("Initial request received",
#                 source=SystemEnum.User,
#                 destination=SystemEnum.FactCheckerAPI,
#                 info=full_request_info.dict())
    
#     open_ai_client = OpenAI(api_key="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")
#     assistant = open_ai_client.beta.assistants.retrieve("asst_UcIkdRNM0toRY0Iu6BfP0eDY")
#     thread = open_ai_client.beta.threads.create()

#     query_string = "Verify: " + selection

#     open_ai_client.beta.threads.messages.create(
#         thread_id=thread.id,
#         role="user",
#         content=query_string
#     )

#     run = open_ai_client.beta.threads.runs.create(
#         thread_id=thread.id,
#         assistant_id=assistant.id,
#         instructions="Verify that the users' statement is true or false. Answer with a tuple, where the first value is 'true', 'false' or 'unknown', the second is a reasoning, and the third is the number of the wiki lookup result to use. For example, [\"true\", \"The vikings are originally from the area of Denmark.\", 1]. You must use the get_wiki_article tool to get the reasoning.",
#     )

#     # Run the assistant by keeping pull requesting the current state, every second
#     run = _run_chat_gpt_thread(open_ai_client=open_ai_client,
#                                 thread_id=thread.id,
#                                 run_id=run.id)

#     required_action = run.required_action
#     if required_action == None:
#         return JSONResponse(content={"result": "unknown", "explanation": "ChatGPT failed to return a required action"})


#     tool_call = required_action.submit_tool_outputs.tool_calls[0]

#     srsearch_param_regex = re.compile('"srsearch": "([^"]+)"')

#     keywords = srsearch_param_regex.search(tool_call.function.arguments).group(1)
#     if keywords == None:
#         return JSONResponse(content={"result": "unknown", "explanation": "ChatGPT failed to return a search term"})

#     wiki_search_info = SearchWikipediaParams(keywords=keywords,
#                                              language="en",
#                                              srsearch=urllib.parse.quote(keywords))
    
#     logger.info("Starting wikipedia search",
#                 source=SystemEnum.FactCheckerAPI,
#                 destination=SystemEnum.Wikipedia,
#                 info=wiki_search_info.dict())


#     wiki_res = _search_wiki(search_params=wiki_search_info)

#     if (wiki_res.status_code != 200):
#         return JSONResponse(content={"result": "unknown", "explanation": "Wikipedia search failed."})
    
#     wiki_res_json = wiki_res.json()

#     if (wiki_res_json == None or not "query" in wiki_res_json or not "search" in wiki_res_json['query']):
#         return JSONResponse(content={"result": "unknown", "explanation": "Wikipedia search failed."})
    
#     logger.info("Wikipedia search result",
#                 source=SystemEnum.Wikipedia,
#                 destination=SystemEnum.FactCheckerAPI,
#                 info=wiki_res_json)

#     articles = wiki_res_json['query']['search']

#     article_list = _extract_article_list(articles)

#     tool_call_info = ChatGPTToolCallResult(output="\n".join(article_list),
#                                              tool_call_id=tool_call.id)
    
#     logger.info("Sending wiki search results to ChatGPT",
#                 source=SystemEnum.FactCheckerAPI,
#                 destination=SystemEnum.ChatGPT,
#                 info=tool_call_info.dict())

#     # Send the article list to ChatGPT
#     open_ai_client.beta.threads.runs.submit_tool_outputs(
#         thread_id=thread.id,
#         run_id=run.id,
#         tool_outputs=[{
#             'tool_call_id': tool_call_info.tool_call_id,
#             'output': tool_call_info.output      
#         }]
#     )

#     # Run the assistant by keeping pull requesting the current state, every second
#     run = _run_chat_gpt_thread(open_ai_client=open_ai_client,
#                                 thread_id=thread.id,
#                                 run_id=run.id)

#     messages = open_ai_client.beta.threads.messages.list(
#         thread_id=thread.id
#     )

#     return _extract_response_from_chat_gpt_response(messages=messages, articles=articles)


# def _create_response(wiki_article: FactCheckerArticleResponse | None,
#                      explanation: str,
#                      result: FactCheckResult) -> FactCheckerResponse:
#     response = FactCheckerResponse(article=wiki_article,
#                                explanation=explanation,
#                                result=result)
#     logger.info("Sending response back to to the user",
#                 source=SystemEnum.FactCheckerAPI,
#                 destination=SystemEnum.User,
#                 info=response.dict())
#     return response

# def _extract_response_from_chat_gpt_response(messages: Any, articles: Any) -> FactCheckerResponse:
#     result_txt = messages.data[0].content[0].text.value

#     try:
#         result_json = json.loads(result_txt)
#     except:
#         print(result_txt)
#         return JSONResponse(content={"result": "unknown", "reasoning": "Invalid result format."})

#     if (result_json == None or len(result_json) == 0):
#         return JSONResponse(content={"result": "unknown", "reasoning": "No results found."})
    
#     relevant_quote_index = result_json[2] - 1
#     if (relevant_quote_index < 0 or relevant_quote_index >= len(articles)):
#         return JSONResponse(content={"result": "unknown", "reasoning": "Invalid result index."})
    
#     relevant_search_result = articles[relevant_quote_index]
#     wiki_article = FactCheckerArticleResponse(url="http://en.wikipedia.org/?curid=" + str(relevant_search_result['pageid']),
#                                     title=relevant_search_result['title'],
#                                     snippet=relevant_search_result['snippet'])

#     response_data = _create_response(wiki_article=wiki_article,result=result_json[0],explanation=result_json[1])
#     return JSONResponse(content=response_data.dict())

# def _extract_article_list(searchResults: Any) -> list[str]:
#     articleList = []
#     for i in range(len(searchResults)):
#         articleList.append(str(i + 1) + ' - ' + searchResults[i]['title'] + ': ' + searchResults[i]['snippet'])
#     return articleList

# def _search_wiki(search_params=SearchWikipediaParams) -> Response:
#     url_base = f'https://en.wikipedia.org/w/api.php?action=query&format=json&formatversion=2&list=search'
#     search_url = f'{url_base}&uselang={search_params.language}&srsearch={search_params.srsearch}'
#     logger.info("Sending request to Wikipedia",
#             source=SystemEnum.FactCheckerAPI,
#             destination=SystemEnum.Wikipedia,
#             info={"url": search_url})
#     return requests.get(search_url, timeout=10)

# def _run_chat_gpt_thread(open_ai_client: Any,
#                           thread_id: str,
#                           run_id: str) -> Any:
#     while True:
#         run = open_ai_client.beta.threads.runs.retrieve(
#             thread_id=thread_id,
#             run_id=run_id
#         )
#         # sleep one second
#         if run.status != "in_progress":
#             break
#         sleep(1)

#     return run
    

