from typing import Annotated
from app.prompt_constants import FLUENT_KW_PROMPT, FLUENT_QUOTE_PROMPT, KEYWORD_AND_SECTION_EXTRACTION_INSTRUCTIONS
from app.services.chatgpt_llm_service import ChatGPTLLMService
from app.services.citation_finder import CitationFinderService
from app.services.wikipedia_search_service import WikipediaSearchService
from app.services.xtools_article_details_service import XtoolsArticleDetailsService
from app.utils.settings import get_settings
from fastapi.responses import StreamingResponse
from fastapi import APIRouter, Depends, HTTPException, status, Header
from structlog import getLogger
from fastapi.responses import JSONResponse

from app import constants
from app.models.parameters import (FactCheckerRequestInfo)
from app.services.feature_flagging_service import get_feature_flagging_service
from app.utils.settings_types import LLMInteractionStrategy, SearchStrategy, SystemEnum
import re
import requests
from requests import Response
import urllib.parse
import json
router = APIRouter()

logger = getLogger(__name__)
    
@router.get('/find_citation')
async def find_citation(selection: str, 
                        context: str | None = None,
                        x_user_id: Annotated[str | None, Header()] = None,
                        x_site_category: Annotated[str | None, Header()] = None) -> StreamingResponse:
    feature_flagging_service = get_feature_flagging_service()
    
    settings = get_settings()

    if not feature_flagging_service.api_is_enabled():
        raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE, detail="API_DISABLED")

    # enforce maxmimum input length
    selection = selection.strip()[0:constants.MAX_INPUT_LENGTH]
    context = context.strip()[0:constants.MAX_INPUT_LENGTH] if context else None

    user_id = x_user_id if x_user_id != None else "anonymous"
    full_request_info = FactCheckerRequestInfo(selection=selection,
                                               context=context,
                                               client_id=user_id, 
                                               site_category=x_site_category)

    
    if feature_flagging_service.user_is_on_blocklist(user_id=user_id):
        logger.info("User is on blocklist",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=full_request_info.dict())
        raise HTTPException(status_code=status.HTTP_429_TOO_MANY_REQUESTS, detail="USER_BLOCKED")
    
    # make sure that the provided claim is not empty. Consider white spaces as empty
    if not selection or not selection.strip() or selection.isspace():
        logger.info("Empty claim provided",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=full_request_info.dict())
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="EMPTY_CLAIM")

    # make sure that the provided claim is not longer than 600 characters. The frontend uses 
    # a limit of 300 characters, but we are using 600 characters to be on the safe side with encodings
    if len(selection) > 600:
        logger.info("Claim too long",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=full_request_info.dict())
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="CLAIM_TOO_LONG")

    logger.info("Initial request received",
                source=SystemEnum.User,
                destination=SystemEnum.CitationNeededAPI,
                info=full_request_info.dict())
    
    fact_checker_service = CitationFinderService(search_strategy=SearchStrategy.TocAndSections,
                                                llm_service=ChatGPTLLMService(
                                                    interaction_strategy=LLMInteractionStrategy.JSONMode,
                                                    model_version=(settings.model_override or "gpt-4-turbo-2024-04-09"),
                                                    seed=12345),
                                                search_service=WikipediaSearchService(),
                                                article_details_service=XtoolsArticleDetailsService(),
                                                feature_flagging_service=get_feature_flagging_service(),
                                                instruction_keywords=FLUENT_KW_PROMPT,
                                                instruction_quote=FLUENT_QUOTE_PROMPT)
    
    return StreamingResponse(fact_checker_service.stream_citation_finding_no_section_selection(full_request_info),
                             media_type='application/x-ndjson')
    
@router.get('/find_citation_customized')
async def find_citation_customized(selection: str,
                                    model_version: str,
                                    instructions_keywords: str,
                                    instructions_sections: str,
                                    instructions_quote: str,
                                    x_user_id: Annotated[str | None, Header()] = None,
                                    x_user_secret: Annotated[str | None, Header()] = None,
                                    x_site_category: Annotated[str | None, Header()] = None) -> StreamingResponse:
    
    selection = selection.strip()[0:constants.MAX_INPUT_LENGTH]

    user_id = x_user_id if x_user_id != None else "anonymous"
    full_request_info = FactCheckerRequestInfo(selection=selection, 
                                               client_id=user_id, 
                                               site_category=x_site_category)
    
    settings = get_settings()
    if x_user_secret == None or x_user_secret != settings.extended_call_secret:
        logger.info("Unauthorized request received",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=full_request_info.dict())
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="UNAUTHORIZED")

    feature_flagging_service = get_feature_flagging_service()
    
    if not feature_flagging_service.api_is_enabled():
        logger.info("API is disabled",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=full_request_info.dict())
        raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE, detail="API_DISABLED")
    
    if feature_flagging_service.user_is_on_blocklist(user_id=user_id):
        logger.info("User is on blocklist",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=full_request_info.dict())
        raise HTTPException(status_code=status.HTTP_429_TOO_MANY_REQUESTS, detail="USER_BLOCKED")

    # make sure that the provided claim is not empty. Consider white spaces as empty
    if not selection or not selection.strip() or selection.isspace():
        logger.info("Empty claim provided",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=full_request_info.dict())
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="EMPTY_CLAIM")

    # make sure that the provided claim is not longer than 600 characters. The frontend uses 
    # a limit of 300 characters, but we are using 600 characters to be on the safe side with encodings
    if len(selection) > 600:
        logger.info("Claim too long",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=full_request_info.dict())
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="CLAIM_TOO_LONG")

    logger.info("Initial request received",
                source=SystemEnum.User,
                destination=SystemEnum.CitationNeededAPI,
                info=full_request_info.dict())
    
    # extract model version from the request, fall back to gpt-3.5-turbo-0125 if not provided
    model_version = model_version if model_version != None else (settings.model_override or "gpt-3.5-turbo-0125")

    fact_checker_service = CitationFinderService(search_strategy=SearchStrategy.TocAndSections,
                                                llm_service=ChatGPTLLMService(
                                                    interaction_strategy=LLMInteractionStrategy.JSONMode,
                                                    model_version=model_version,
                                                    seed=12345),
                                                search_service=WikipediaSearchService(),
                                                article_details_service=XtoolsArticleDetailsService(),
                                                feature_flagging_service=get_feature_flagging_service(),
                                                instruction_keywords=instructions_keywords,
                                                instruction_sections=instructions_sections,
                                                instruction_quote=instructions_quote)
    
    return StreamingResponse(
        fact_checker_service.stream_citation_finding(full_request_info), 
        media_type='application/x-ndjson')


