"""Utilities for working with python packages"""

from importlib.resources import files
from pathlib import Path


def get_package_path(file_path: str,
                     package_name: str = "app",) -> Path:
    """Returns the path to a file in the package"""
    return Path(str(files(package_name) / file_path))
