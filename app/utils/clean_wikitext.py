import mwparserfromhell
from mwparserfromhell.wikicode import Wikicode
from mwparserfromhell.nodes._base import Node
from mwparserfromhell.nodes.text import Text
from mwparserfromhell.nodes.template import Template
from mwparserfromhell.nodes.comment import Comment
from mwparserfromhell.nodes.tag import Tag
from mwparserfromhell.nodes.wikilink import Wikilink
from mwparserfromhell.nodes.html_entity import HTMLEntity
from mwparserfromhell.nodes.heading import Heading
from mwparserfromhell.nodes.external_link import ExternalLink
import html

import structlog

logger = structlog.getLogger(__name__)


def _wiki_table_to_html(node):
    result = ['<table>']
    for row in node.contents.nodes:
        if isinstance(row, Tag) and row.tag == 'tr':
            result.append('<tr>')
            for cell in row.contents.nodes:
                if isinstance(cell, Tag) and cell.tag in ['td', 'th']:
                    result.append(f'<{cell.tag}>')
                    result.append(_strip_code(cell.contents))
                    result.append(f'</{cell.tag}>')
            result.append('</tr>')
    result.append('</table>')
    return ''.join(result)

def _strip_tag(w: Tag) -> str:
    """
    Function that takes a Tag object and returns a string with all the markup removed.
    """

    match str(w.tag).lower():
        case "code":
            return "`" + _strip_code(w.contents) + "`"
        case "syntaxhighlight":
            return "```" + _strip_code(w.contents) + "```"
        case "b" | "i" | "strong" | "em":
            return _strip_code(w.contents)
        case "li":
            return " - " + _strip_code(w.contents)
        case "ref" | "references" | "gallery" | "mapframe" | "wbr":
            # drop altogether
            return ""
        case "table":
            return _wiki_table_to_html(w)
        case "tr" | "td" | "th" :
            raise Exception(f"Support for tag {w.tag} not implemented in this method")
        case "br" | "hr":
            return "\n"
        case "nowiki":
            return str(w.contents)
        case "dt" | "onlyinclude" | "noinclude" | "includeonly" | "div" | "span" | "p":
            return _strip_code(w.contents)
        case "dd":
            return "\t" + _strip_code(w.contents)
        case "sup" | "sub" | "math" | "small" | "hiero" | "score" | "del" | "ins" | "pre" | "poem" | "big":
            return f"<{w.tag}>{_strip_code(w.contents)}</{w.tag}>"
        case "blockquote":
            return f"> {_strip_code(w.contents)}"

        case _:
            raise Exception(f"Support for tag {w.tag} holding {(w.contents)} not implemented")

def _strip_code(w: Wikicode) -> str:
    """
    Function that takes a Wikicode object and returns a string with all the markup removed.
    """

    stripped_texts = []
    for n in w.nodes:
        match n:
            case None:
                pass
            case Tag():
                stripped_texts.append(_strip_tag(n))
            case Text():
                stripped_texts.append(n.value)
            case Template():
                pass
            case Comment():
                pass
            case Tag():
                stripped_texts.append(_strip_tag(n))
            case Wikilink():
                target = str(n.title)
                if target.startswith("File:") or target.startswith("Image:") or target.startswith("Category:"):
                    continue
                display = n.text or n.title
                stripped_texts.append(_strip_code(display))
            case HTMLEntity():
                stripped_texts.append(html.unescape(n.value))
            case Heading():
                stripped_texts.append("#" * n.level + " " + _strip_code(n.title))
            case ExternalLink():
                if n.title:
                    stripped_texts.append(_strip_code(n.title))
            case _:
                raise Exception(f"Support for node {n} of type {type(n)} not implemented")
    return "".join(stripped_texts)


def clean_wikitext(wikitext: str) -> str:
    """
    Function that takes wikimarkup text and produces cleaned text that is appropriate for NLP tasks.
    """
    wt = mwparserfromhell.parse(wikitext)
    try:
        txt = _strip_code(wt)
        while "\n\n\n" in txt:
            txt = txt.replace("\n\n\n", "\n\n")
        return txt
    except Exception as e:
        logger.warn(f"Error cleaning wikitext: {e}")
        return wt.strip_code()


if __name__ == "__main__":
    # testing driver function with random wikipedia articles
    import requests
    import time

    before_sizes = []
    after_sizes = []
    all_wikitext = ""
    all_plaintext = ""
    for i in range(1000):
        random_res = requests.get("https://en.wikipedia.org/w/api.php?action=query&list=random&rnnamespace=0&rnlimit=2&format=json").json()
        title = random_res['query']['random'][0]['title']
        print(title)
        res = requests.get(f'https://en.wikipedia.org/w/api.php?action=query&prop=revisions&titles={title}&rvslots=*&rvprop=content&formatversion=2&format=json')
        data = res.json()
        wikitext = data['query']['pages'][0]['revisions'][0]['slots']['main']['content']
        before_sizes.append(len(wikitext))
        all_wikitext += wikitext
        plain = clean_wikitext(wikitext)
        all_plaintext += plain
        after_sizes.append(len(plain))
        print(plain)    
        time.sleep(1)

    print(f"Average reduction in size: {(sum(before_sizes) - sum(after_sizes)) / len(before_sizes)}")
