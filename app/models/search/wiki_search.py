from pydantic import BaseModel, Field


class WikiPage(BaseModel):
    ns: int
    title: str
    missing: bool | None
    contentmodel: str | None
    pagelanguage: str | None
    pagelanguagehtmlcode: str | None
    pagelanguagedir: str | None
    pageid: int | None
    touched: str | None
    lastrevid: int | None
    length: int | None
    fullurl: str | None
    editurl: str | None
    canonicalurl: str | None
    pageprops: dict[str, str] | None
    index: int | None


class Normalization(BaseModel):
    fromencoded: str
    from_: str = Field(..., alias="from")
    to: str


class Redirect(BaseModel):
    from_: str = Field(..., alias="from")
    to: str


class WikiQuery(BaseModel):
    pages: list[WikiPage] | None
    normalized: list[Normalization] | None
    redirects: list[Redirect] | None


class WikiAPIResponse(BaseModel):
    warnings: dict[str, dict[str, str]] | None
    batchcomplete: bool
    continue_: dict[str, str] | None = Field(alias="continue")
    query: WikiQuery | None
