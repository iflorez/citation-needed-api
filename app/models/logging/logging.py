from datetime import datetime
from typing import Any

from pydantic import BaseModel

from app.models.parameters import ChatGPTRequest, FactCheckerResponse
from app.models.search.search import (SearchResult, WikipediaSearchResult)
from app.utils.settings_types import SystemEnum


class BaseLogMessage(BaseModel):
    event: str
    logger: str
    level: str
    timestamp: datetime
    filename: str | None
    lineno: int | None
    func_name: str | None
    request_id: str | None
    color_message: str | None


class HTTPInfo(BaseModel):
    url: str
    status_code: int
    method: str
    request_id: str
    version: str


class HTTPLogMessage(BaseLogMessage):
    http: HTTPInfo
    duration: int


class CustomLogMessage(BaseLogMessage):
    source: SystemEnum | None
    destination: SystemEnum | None
    info: Any


class RequestLogMessage(CustomLogMessage):
    info: ChatGPTRequest


class SearchLogMessage(CustomLogMessage):
    info: SearchResult


class WikipediaSearchLogMessage(SearchLogMessage):
    info: WikipediaSearchResult


class ResponseLogMessage(CustomLogMessage):
    info: FactCheckerResponse
