
from app.services.interfaces import FeatureFlaggingService


class MockFeatureFlaggingService(FeatureFlaggingService):
    def api_is_enabled(self) -> bool:
        return True
    
    def show_fact_check_result(self) -> bool:
        return True
    
    def show_fact_check_explanation(self) -> bool:
        return True
    
    def user_is_on_blocklist(self, user_id: str) -> bool:
        return False