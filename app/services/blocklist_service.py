from functools import lru_cache

from fastapi import HTTPException, status
from structlog import getLogger

from app.services.feature_flagging_service import (
    UnleashFeatureFlaggingService, get_feature_flagging_service)

logger = getLogger(__name__)


class BlocklistService:
    def __init__(self, feature_flagging_service: UnleashFeatureFlaggingService):
        self.feature_flagging_service = feature_flagging_service

    def check_blocklist_for_user(self, user_ip: str | None) -> None:
        if user_ip and self.feature_flagging_service.user_is_on_blocklist(user_ip=user_ip):
            raise HTTPException(status_code=status.HTTP_429_TOO_MANY_REQUESTS, detail="User blocked")


@lru_cache()
def get_blocklist_service() -> BlocklistService:
    return BlocklistService(feature_flagging_service=get_feature_flagging_service())
