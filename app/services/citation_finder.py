import asyncio
from concurrent.futures import ThreadPoolExecutor
from app.models.llm.interaction import QuoteSelectionResult
from app.services.errors import LLMInteractionError
from app.services.interfaces import ArticleDetailsService, FeatureFlaggingService, LLMService, SearchService
from structlog import getLogger
from typing import Any
from threading import Thread
from app import constants
from app.utils.settings_types import  SearchStrategy, SystemEnum
from app.models.parameters import FactCheckerRequestInfo
import json

logger = getLogger(__name__)

def _run_async_in_new_pool(coro):
    """
    Function that is kicked off in a new thread to run an async function in a new event loop.
    Returns whatever the async coroutine returns upon awaiting.
    """
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    resp = loop.run_until_complete(coro)
    loop.close()
    return resp



class CitationFinderService:
    """Fetches articles from Wikipedia and interacts with ChatGPT to find citations for a given query."""
    def __init__(self,
                 search_strategy: SearchStrategy,
                 llm_service: LLMService,
                 search_service: SearchService,
                 article_details_service: ArticleDetailsService,
                 feature_flagging_service: FeatureFlaggingService,
                 instruction_keywords: str | None = None,
                 instruction_sections: str | None = None,
                 instruction_quote: str | None = None):
        self._pool = ThreadPoolExecutor(max_workers=2)
        self.search_strategy = search_strategy
        self.llm_service = llm_service
        self.search_service = search_service
        self.article_details_service = article_details_service
        self.instruction_keywords = instruction_keywords
        self.instruction_sections = instruction_sections
        self.instruction_quote = instruction_quote
        self.feature_flagging_service = feature_flagging_service

    async def stream_citation_finding(self, info: FactCheckerRequestInfo) -> Any:
        logger.info("User request received",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=info.dict())
        
        self.llm_service.set_search_language("en")

        yield json.dumps({"current_step": "Checking out the statement"}) + '\n'

        try:
            keyword_result = self.llm_service.extract_keyword(selection=info.selection,
                                                              context=info.context,
                                                              instructions=self.instruction_keywords)
            
            search_term = " ".join(keyword_result.keywords)

            yield json.dumps({"current_step": f"Picking out key words"}) + '\n'

            wiki_search_results = self.search_service.search_articles(search_term=search_term, search_language="en")
            
            parsed_articles = wiki_search_results.parsed_results

            if (parsed_articles == None or len(parsed_articles) == 0):
                yield json.dumps({"result": "no_articles", "explanation": "No matching Wikipedia pages found."})
                return
            
            top_parsed_article = parsed_articles[0]

            tocs = self.search_service.fetch_tables_of_content(articles=parsed_articles, keywords=keyword_result.keywords, search_language="en")

            yield json.dumps({"current_step": "Looking up on Wikipedia"}) + '\n'

            selected_sections_result = self.llm_service.select_sections(tocs=tocs, 
                                                                        selection=info.selection,
                                                                        instructions=self.instruction_sections)
            selected_sections = selected_sections_result.sections
            selected_sections_string = []

            if (selected_sections == None or len(selected_sections) == 0):
                # if no sections are found, we return a no result message but add the top article, 
                # which will still be shown to the user
                article_lead_section = self.search_service.fetch_sections(sections=[(top_parsed_article.title, constants.LEAD_SECTION_PLACEHOLDER)], search_language="en")
                article_details = self.article_details_service.get_details(page_title=top_parsed_article.title, search_language="en")
                
                # We take the first 200 characters of the lead section, broken at a whitespace
                lead_section_snippet = article_lead_section[0][2]
                lead_section_end_pos = lead_section_snippet.find(' ', 200)
                if lead_section_end_pos == -1:
                    lead_section_end_pos = 200
                lead_section_snippet = article_lead_section[0][2][:lead_section_end_pos] + "..."
                result_article = self.search_service.create_result_article(
                    (top_parsed_article.title, constants.LEAD_SECTION_PLACEHOLDER, lead_section_snippet), 
                    article_details=article_details)
                result_json ={"result": "no_result", 
                                  "explanation": "No matching sections found.",
                                  "article": result_article.dict()}
                logger.info("No sections selected response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info=result_json)
                yield json.dumps(result_json)
                return

            for section in selected_sections:
                selected_sections_string.append(f'"{section[0]}: {section[1]}"')

            sections = self.search_service.fetch_sections(sections=selected_sections, search_language="en")

            yield json.dumps({"current_step": "Finding something for you"}) + '\n'

            selected_quote_result = self.llm_service.select_quote(sections=sections, 
                                                                  selection=info.selection,
                                                                  context=info.context,
                                                                  instructions=self.instruction_quote)
            selected_quote = selected_quote_result.quote

            if (selected_quote == None):
                # if no mtaching quote is found, we return a no result message but add the top 
                # article, which will still be shown to the user
                article_lead_section = self.search_service.fetch_sections(sections=[(top_parsed_article.title, constants.LEAD_SECTION_PLACEHOLDER)], search_language="en")
                article_details = self.article_details_service.get_details(page_title=top_parsed_article.title, search_language="en")
                
                # We take the first 200 characters of the lead section, broken at a whitespace
                lead_section_snippet = article_lead_section[0][2]
                lead_section_end_pos = lead_section_snippet.find(' ', 200)
                if lead_section_end_pos == -1:
                    lead_section_end_pos = 200
                lead_section_snippet = article_lead_section[0][2][:lead_section_end_pos] + "..."
                result_article = self.search_service.create_result_article(
                    (top_parsed_article.title, constants.LEAD_SECTION_PLACEHOLDER, lead_section_snippet), 
                    article_details=article_details)

                result_json = {"result": "no_result", 
                               "explanation": "No matching citations found.",
                               "article": result_article.dict()}
                logger.info("No quote found response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info=result_json)
                yield json.dumps(result_json)
                return
            
            selected_page_title = selected_quote[0]

            article_details = None
            try:
                article_details = self.article_details_service.get_details(page_title=selected_page_title, search_language="en")
            except Exception as e:
                logger.info("Error while fetching article details",
                            source=SystemEnum.XTools,
                            destination=SystemEnum.CitationNeededAPI,
                            info={"error": str(e)})

            result_article = self.search_service.create_result_article(selected_quote, article_details=article_details)

            # if we are allowed to show a fact check result and explanation, we do so

            if self.feature_flagging_service.show_fact_check_result() \
                and self.feature_flagging_service.show_fact_check_explanation() \
                    and selected_quote_result.result != None \
                    and selected_quote_result.explanation != None:
                result_json = {"result": selected_quote_result.result, 
                               "explanation": selected_quote_result.explanation,
                               "article": result_article.dict()}
            # if we are only allowed to show a fact check result, we do so
            elif self.feature_flagging_service.show_fact_check_result() and selected_quote_result.result != None:
                result_json = {"result": selected_quote_result.result, 
                               "article": result_article.dict()}
            else:
                result_json = {"result": "quote_found", 
                               "explanation": "A matching citation was found in an article", 
                               "article": result_article.dict()}

            logger.info("Successfull response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info=result_json)
            yield json.dumps(result_json)

            return
        # Handle all llm interaction errors - return a generic error message but log in detail
        except LLMInteractionError as e:
            result_json = {"result": "error", 
                           "explanation": f"Troubles while interacting with ChatGPT: {e}"}
            
            logger.info("Error response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info={"result": "error",
                              "explanation": f"Troubles while interacting with ChatGPT: {e}",
                              "error": str(e)})
            
            yield json.dumps(result_json)
            return
        # Handle all other errors - return a generic error message but log in detail
        except Exception as e:
            result_json = {"result": "error", "explanation": "Something unexpected happened."}
            
            logger.info("Error response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info={"result": "error", 
                              "explanation": "Something unexpected happened.",
                              "error": str(e)})
            
            yield json.dumps(result_json)
            return
        

    async def stream_citation_finding_no_section_selection(self, info: FactCheckerRequestInfo) -> Any:
        logger.info("User request received",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=info.dict())
        
        self.llm_service.set_search_language("en")

        yield json.dumps({"current_step": "Checking out the statement"}) + '\n'

        try:
            keyword_result = self.llm_service.extract_keyword(selection=info.selection,
                                                              context=info.context,
                                                              instructions=self.instruction_keywords)

            yield json.dumps({"current_step": f"Picking out key words"}) + '\n'


            search_terms = keyword_result.keywords
            if keyword_result.search_terms != None:
                search_terms = keyword_result.search_terms

            articles = self.search_service.search_articles(search_term=keyword_result.search_terms[0])

            combined_articles = articles.parsed_results

            if (combined_articles == None or len(combined_articles) == 0):
                yield json.dumps({"result": "no_articles", "explanation": "No matching Wikipedia pages found."})
                return

            # deduplicate the articles
            unique_articles = []
            for article in combined_articles:
                # check articles by title
                if article.title not in [a.title for a in unique_articles]:
                    unique_articles.append(article)

            unique_article_titles = [a.title for a in unique_articles]
            article_details_fut = self._pool.submit(_run_async_in_new_pool, self.article_details_service.get_batch_details(unique_article_titles, search_language="en"))


            combined_keywords = None
            # append synonyms to the keywords if any were found
            if keyword_result.synonyms != None:
                combined_keywords = keyword_result.keywords + keyword_result.synonyms + search_terms
            else:
                combined_keywords = keyword_result.keywords + search_terms

            # Remove any duplicates in the combined keywords
            combined_keywords = list(set(combined_keywords))
            
            top_parsed_article = unique_articles[0]

            yield json.dumps({"current_step": "Looking up on Wikipedia"}) + '\n'

            tocs = self.search_service.fetch_tables_of_content(articles=unique_articles, keywords=combined_keywords, search_language="en")

            # flatten the list(tuples(str, list(tuple(str, str))) into a simple list(tuple(str, str, str))
            flat_tocs = []
            for article, sections in tocs:
                for section_title, section_keyword_value in sections:
                    flat_tocs.append((article, section_title, section_keyword_value))

            # create a list of the top 3 most relevant sections. We do this by looking at the number of times the keyword appears in the section
            sorted_tocs = sorted(flat_tocs, key=lambda x: x[2], reverse=True)
            # remove the keyword_values from the list
            sorted_tocs = [(x[0], x[1]) for x in sorted_tocs]

            sorted_sections=sorted_tocs[:3]

            yield json.dumps({"current_step": "Finding something for you"}) + '\n'

            sections = self.search_service.fetch_sections(sections=sorted_sections, search_language="en")

            selected_quote_result: QuoteSelectionResult = self.llm_service.select_quote(sections=sections,
                                                                                        selection=info.selection,
                                                                                        context=info.context,
                                                                                        instructions=self.instruction_quote)
            selected_quote = selected_quote_result.quote

            if (selected_quote == None):
                # if no mtaching quote is found, we return a no result message but add the top 
                # article, which will still be shown to the user
                article_lead_section = self.search_service.fetch_sections(sections=[(top_parsed_article.title, constants.LEAD_SECTION_PLACEHOLDER)], search_language="en")

                article_details = None
                try:
                    article_details_list = article_details_fut.result()
                    details_idx = unique_article_titles.index(top_parsed_article.title)
                    article_details = article_details_list[details_idx]
                except Exception as e:
                    logger.info("Error while fetching article details",
                                source=SystemEnum.XTools,
                                destination=SystemEnum.CitationNeededAPI,
                                info={"error": str(e)})
                
                # We take the first 200 characters of the lead section, broken at a whitespace
                lead_section_snippet = article_lead_section[0][2]
                lead_section_end_pos = lead_section_snippet.find(' ', 200)
                if lead_section_end_pos == -1:
                    lead_section_end_pos = 200
                lead_section_snippet = article_lead_section[0][2][:lead_section_end_pos] + "..."
                result_article = self.search_service.create_result_article(
                    (top_parsed_article.title, constants.LEAD_SECTION_PLACEHOLDER, lead_section_snippet), 
                    article_details=article_details)

                result_json = {"result": "no_result",
                               "explanation": "No matching citations found.",
                               "article": result_article.dict()}
                logger.info("No quote found response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info=result_json)
                yield json.dumps(result_json)
                return

            selected_page_title = selected_quote[0]
            article_details = None
            try:
                article_details_list = article_details_fut.result()
                details_idx = unique_article_titles.index(selected_page_title)
                article_details = article_details_list[details_idx]

            except Exception as e:
                logger.info("Error while fetching article details",
                            source=SystemEnum.XTools,
                            destination=SystemEnum.CitationNeededAPI,
                            info={"error": str(e)})


            result_article = self.search_service.create_result_article(selected_quote, article_details=article_details)

            # if we are allowed to show a fact check result and explanation, we do so

            if self.feature_flagging_service.show_fact_check_result() \
                and self.feature_flagging_service.show_fact_check_explanation() \
                    and selected_quote_result.result != None \
                    and selected_quote_result.explanation != None:
                result_json = {"result": selected_quote_result.result,
                               "explanation": selected_quote_result.explanation,
                               "article": result_article.dict()}
            # if we are only allowed to show a fact check result, we do so
            elif self.feature_flagging_service.show_fact_check_result() and selected_quote_result.result != None:
                result_json = {"result": selected_quote_result.result,
                               "article": result_article.dict()}
            else:
                result_json = {"result": "quote_found",
                               "explanation": "A matching citation was found in an article",
                               "article": result_article.dict()}

            logger.info("Successfull response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info=result_json)
            yield json.dumps(result_json)

            return
        # Handle all llm interaction errors - return a generic error message but log in detail
        except LLMInteractionError as e:
            result_json = {"result": "error", 
                           "explanation": f"Troubles while interacting with ChatGPT: {e}"}
            
            logger.info("Error response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info={"result": "error",
                              "explanation": f"Troubles while interacting with ChatGPT: {e}",
                              "error": str(e)})
            
            yield json.dumps(result_json)
            return
        # Handle all other errors - return a generic error message but log in detail
        except Exception as e:
            result_json = {"result": "error", "explanation": "Something unexpected happened."}
            
            logger.info("Error response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info={"result": "error", 
                              "explanation": "Something unexpected happened.",
                              "error": str(e)})
            
            yield json.dumps(result_json)
            return