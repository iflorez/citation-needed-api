import csv
import logging
from typing import Annotated

import typer

from common.models.conversation import Conversation
from common.log_parser import LogParser
from common.services import GoogleSheetsService

app = typer.Typer()

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


@app.command()
def parse_log_file(input_file: Annotated[
                    str,
                    typer.Option(help="The input file")
                ] = '',
                input_sheet: Annotated[
                    str,
                    typer.Option(help="The input sheet in Google Sheets")
                ] = '',
                output_file: Annotated[
                    str,
                    typer.Option(help="The output file")
                ] = 'results_parsed.tsv',
                credentials_file: Annotated[
                    str,
                    typer.Option(help="Path to service account credentials file")
                ] = 'credentials.json',
                write_to_gsheets: Annotated[
                    bool,
                    typer.Option(help="Option to write the results to Google Sheets")
                ] = False,
                spreadsheet_id: Annotated[
                    str,
                    typer.Option(help="ID of the Google Sheets spreadsheet")
                ] = '1ZERcg6bvN8PDicB_h1TCzYYE0IAgQ0Fca0WE-Hc4TeE',
                sheet_title: Annotated[
                    str,
                    typer.Option(help="Name of the Google Sheets sheet")
                ] = '',
                output_sheet: Annotated[
                    str,
                    typer.Option(help="The name of the output sheet in Google Sheets")
                ] = ''):
    logger.info(f"""Script called with arguments:
                input-file: {input_file},
                input-sheet: {input_sheet},
                output-file: {output_file},
                output-sheet: {output_sheet},
                credentials-file: {credentials_file},
                write_to_gsheets: {write_to_gsheets},
                spreadsheet_id: {spreadsheet_id},
                sheet_title: {sheet_title}""")
    if input_sheet == '' and input_file != '':
        logger.info(f"Input sheet not provided. Reading from file {input_file}...")
        log_parser = LogParser.from_file(input_file)
    else:
        logger.info(f"Input sheet provided. Obtaining sheet {input_sheet} from {spreadsheet_id}...")
        google_sheets_service = GoogleSheetsService(credentials_file=credentials_file)
        logger.info("Reading from Google Sheets...")
        rows = google_sheets_service.read_from_sheet(spreadsheet_id=spreadsheet_id,
                                                     sheet_title=input_sheet)
        log_parser = LogParser.from_rows(rows)

    logger.info("Parsing logs...")
    conversations = log_parser.parse_logs()
    with open(output_file, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=Conversation.relevant_keys(), delimiter="\t")
        writer.writeheader()
        for conversation in conversations:
            writer.writerow(conversation.relevant_properties)

    if write_to_gsheets:
        if output_sheet == '':
            output_sheet = f"{input_sheet}_parsed"
        google_sheets_service = GoogleSheetsService(credentials_file=credentials_file)
        rows = [conversation.relevant_properties for conversation in conversations]
        logger.info("Writing to Google Sheets...")
        google_sheets_service.write_to_sheet(results=rows,
                                             spreadsheet_id=spreadsheet_id,
                                             sheet_title=output_sheet)


def main():
    app()


if __name__ == '__main__':
    main()
