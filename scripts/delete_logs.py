import datetime
import os.path
import os
import logging

import typer


logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

app = typer.Typer()

@app.command()
def delete_logs():
    """Replaces the default.log file with an empty file"""
    log_path = os.environ.get("TOOL_DATA_DIR") + "/logs"
    log_file = log_path + "/default.log"
    with open(log_file, 'w') as f:
        f.write("")
    logger.info(f"Logs deleted at {datetime.datetime.now()}")
    return


if __name__ == "__main__":
    app()
