""" This script gets logs from AWS CloudWatch Logs and writes them to a file """
import csv
import logging
from datetime import datetime, timedelta

import typer
from typing_extensions import Annotated

from common.services import AWSService, GoogleSheetsService

app = typer.Typer()
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


def write_to_file(results, output_file):
    """ Write results to a file. """
    logger.info("Writing results to %s...", output_file)
    with open(output_file, 'w', newline='', encoding="utf-8") as csvfile:
        writer = csv.writer(csvfile, delimiter='\t')
        writer.writerow(['@timestamp', '@message'])
        for result in results:
            writer.writerow([field['value'] for field in result])


@app.command()
def run_query(
                start_date: Annotated[
                    datetime,
                    typer.Option(
                        formats=["%Y-%m-%d"],
                        help="Start date in YYYY-MM-DD format"
                    ),
                ],
                end_date: Annotated[
                    datetime,
                    typer.Option(
                        formats=["%Y-%m-%d"],
                        help="Start date in YYYY-MM-DD format"
                    ),
                ],
                log_group_name: Annotated[
                    str,
                    typer.Option(help="The name of the log group")
                ] = '/ecs/chatgptplugin_pr_wme',
                query_string: Annotated[
                    str,
                    typer.Option(help="The query string")
                ] = """fields @timestamp, @message
                       | filter @message not like /feature/
                       | filter logger not like "api.access"
                       | sort @timestamp desc
                       | limit 10000""",
                credentials_file: Annotated[
                    str,
                    typer.Option(help="Path to service account credentials file")
                ] = 'credentials.json',
                output_file: Annotated[
                    str,
                    typer.Option(help="The output file")
                ] = 'results.tsv',
                write_to_gsheets: Annotated[
                    bool,
                    typer.Option(help="Option to write the results to Google Sheets")
                ] = False,
                spreadsheet_id: Annotated[
                    str,
                    typer.Option(help="ID of the Google Sheets spreadsheet")
                ] = '1ZERcg6bvN8PDicB_h1TCzYYE0IAgQ0Fca0WE-Hc4TeE',
                sheet_title: Annotated[
                    str,
                    typer.Option(help="Name of the Google Sheets sheet")
                ] = ''
        ):
    """ Accept arguments and run the AWS log query. """
    logger.info("""Script called with arguments:
                start-date: %s
                end-date: %s
                log-group-name: %s
                query-string: %s,
                credentials-file: %s,
                output-file: %s,
                write_to_gsheets: %s,
                spreadsheet_id: %s,
                sheet_title: %s""",
                start_date, end_date, log_group_name, query_string,
                credentials_file, output_file, write_to_gsheets,
                spreadsheet_id, sheet_title)
    aws_service = AWSService()
    range_string = f"{start_date.strftime('%Y-%m-%d')} to {end_date.strftime('%Y-%m-%d')}"
    logger.info("Getting logs from AWS...")

    current_date = start_date
    while current_date <= end_date:
        next_date = current_date + timedelta(days=1)
        range_string = f"{current_date.strftime('%Y-%m-%d')} to {next_date.strftime('%Y-%m-%d')}"
        logger.info("Getting logs from AWS for %s...", range_string)
        results = aws_service.get_logs(start_date=current_date,
                                       end_date=next_date - timedelta(seconds=1),
                                       log_group_name=log_group_name,
                                       query_string=query_string,
                                       cache_file=f"{range_string.replace(' ', '_')}_cache.json")
        if results:
            logger.info("Writing to file %s for %s...", output_file, range_string)
            write_to_file(results, output_file)
            if write_to_gsheets:
                logger.info("Writing to Google Sheets for %s...", range_string)
                if sheet_title == '':
                    sheet_name = range_string
                else:
                    sheet_name = f"{sheet_title} {range_string}"

                google_sheets_service = GoogleSheetsService(credentials_file=credentials_file)
                google_sheets_service.write_to_sheet(results=results,
                                                     spreadsheet_id=spreadsheet_id,
                                                     sheet_title=sheet_name)
        current_date = next_date

    results = aws_service.get_logs(start_date=start_date,
                                   end_date=end_date,
                                   log_group_name=log_group_name,
                                   query_string=query_string,
                                   cache_file=f"{range_string.replace(' ', '_')}_cache.json")
    logger.info("Writing to file %s...", output_file)
    write_to_file(results, output_file)
    if write_to_gsheets:
        logger.info("Writing to Google Sheets...")
        if sheet_title == '':
            sheet_title = range_string
        google_sheets_service = GoogleSheetsService(credentials_file=credentials_file)
        google_sheets_service.write_to_sheet(results=results,
                                             spreadsheet_id=spreadsheet_id,
                                             sheet_title=sheet_title)


if __name__ == "__main__":
    app()
