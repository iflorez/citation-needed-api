""" Service for interacting with Google Sheets """
# pylint: disable=no-member disable=line-too-long

import logging
import os

from google.auth.credentials import Credentials as CredentialsBase
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

logger = logging.getLogger(__name__)


class GoogleSheetsService:
    """ Service for interacting with Google Sheets """
    def __init__(self, credentials_file: str) -> None:
        self.creds = self._authenticate_google_sheets(credentials_file)
        self.service = build('sheets', 'v4', credentials=self.creds)

    def create_spreadsheet(self, spreadsheet_name: str) -> str:
        """ Create a new spreadsheet. """
        spreadsheet_body = {
            'properties': {
                'title': spreadsheet_name
            }
        }

        request = self.service.spreadsheets().create(body=spreadsheet_body)
        response = request.execute()

        logger.info("Created spreadsheet with title %s: %s", spreadsheet_name, response['spreadsheetUrl'])
        return response['spreadsheetId']

    def write_to_sheet(self,
                       results: list[list[str]],
                       spreadsheet_id: str,
                       sheet_title: str) -> None:
        """ Write rows to a Google Sheet. """
        try:
            logger.info("Assembling rows...")
            rows = self._assemble_rows(results)
            sheet_id = self._get_sheet_id(spreadsheet_id, sheet_title)

            if sheet_id is None:
                logger.info("Could not find sheet %s. Creating new sheet...", sheet_title)
                self._create_new_sheet(spreadsheet_id, sheet_title)
                self._update_sheet(spreadsheet_id, sheet_title, rows)
            else:
                logger.info("Found sheet already titled %s. Appending contents", sheet_title)
                self._append_to_sheet(spreadsheet_id, sheet_title, rows)

            logger.info("Results written to Google Sheets: https://docs.google.com/spreadsheets/d/%s/edit", spreadsheet_id)
        except HttpError as err:
            logger.error("Encountered error: %s", err)

    def write_messages_to_sheet(self,
                                results: list[dict[str, str]],
                                spreadsheet_id: str,
                                sheet_title: str,
                                header_row: list[str] = None,
                                setup_properties: bool = False) -> None:
        """ Write messages to a Google Sheet. """
        if header_row is None:
            header_row = ['@timestamp', '@message']
        try:
            logger.info("Assembling rows...")
            rows = self._assemble_rows(results)  # type: ignore # This is technically incorrect but seems to work
            sheet_id = self._get_sheet_id(spreadsheet_id, sheet_title)

            if sheet_id is None:
                logger.info("Could not find sheet %s. Creating new sheet...,", sheet_title)
                self._create_new_sheet(spreadsheet_id, sheet_title)
                rows.insert(0, header_row)
                self._update_sheet(spreadsheet_id, sheet_title, rows)
            else:
                logger.info("Found sheet already titled %s. Appending contents", sheet_title)
                self._append_to_sheet(spreadsheet_id, sheet_title, rows)
            if setup_properties:
                self._setup_sheet_properties(spreadsheet_id, sheet_title)

            logger.info("Results written to Google Sheets: https://docs.google.com/spreadsheets/d/%s/edit", spreadsheet_id)
        except HttpError as err:
            logger.error("Encountered error: %s", err)

    def read_messages_from_sheet(self,
                        spreadsheet_id: str,
                        sheet_title: str) -> list[dict[str, str]]:
        """ Read data from a Google Sheet. """
        result = self.service.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=sheet_title).execute()
        values = result.get('values', [])

        # Convert data to list of dictionaries for compatibility with existing code
        headers = values[0]
        data = [dict(zip(headers, row)) for row in values[1:]]

        return data

    def _authenticate_google_sheets(self, credentials_file: str) -> CredentialsBase:
        logger.info("Authenticating with Google...")
        REQUESTED_SCOPES = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']  # pylint: disable=invalid-name
        creds = None
        token_file = 'token.json'
        if os.path.exists(token_file):
            logger.info("Reading from %s...", token_file)
            creds = Credentials.from_authorized_user_file(token_file, REQUESTED_SCOPES)
        if not creds or not creds.valid:
            logger.info("Credentials not present or not valid. Taking action...")
            if creds and creds.expired and creds.refresh_token:
                logger.info("Credentials expired, refreshing...")
                creds.refresh(Request())
            else:
                logger.info("Obtaining new credentials...")
                flow = InstalledAppFlow.from_client_secrets_file(
                    credentials_file, REQUESTED_SCOPES)
                creds = flow.run_local_server(port=0)
            with open(token_file, 'w', encoding="utf-8") as token:
                logger.info("Writing token file...")
                token.write(creds.to_json())
        return creds

    def _get_sheet_id(self, spreadsheet_id: str, sheet_title: str) -> int | None:
        sheets = self.service.spreadsheets().get(spreadsheetId=spreadsheet_id).execute().get('sheets', '')
        for sheet in sheets:
            if 'properties' in sheet and sheet['properties']['title'] == sheet_title:
                return sheet['properties']['sheetId']

    def _set_column_width(self, spreadsheet_id: str, sheet_title: str, column_index: int, width: int) -> None:
        requests = [{
            "updateDimensionProperties": {
                "range": {
                    "sheetId": self._get_sheet_id(spreadsheet_id, sheet_title),
                    "dimension": "COLUMNS",
                    "startIndex": column_index,
                    "endIndex": column_index + 1
                },
                "properties": {
                    "pixelSize": width
                },
                "fields": "pixelSize"
            }
        }]
        body = {
            'requests': requests
        }
        self.service.spreadsheets().batchUpdate(
            spreadsheetId=spreadsheet_id,
            body=body).execute()

    def _set_text_wrap(self, spreadsheet_id: str, sheet_title: str) -> None:
        requests = [{
            "updateCells": {
                "range": {
                    "sheetId": self._get_sheet_id(spreadsheet_id, sheet_title),
                },
                "fields": "userEnteredFormat.wrapStrategy",
                "rows": [{
                    "values": [{
                        "userEnteredFormat": {
                            "wrapStrategy": "WRAP"
                        }
                    }]
                }]
            }
        }]
        body = {
            'requests': requests
        }
        self.service.spreadsheets().batchUpdate(
            spreadsheetId=spreadsheet_id,
            body=body).execute()

    def _create_new_sheet(self, spreadsheet_id: str, sheet_title: str) -> None:
        body = {
            'requests': [
                {
                    'addSheet': {
                        'properties': {
                            'title': sheet_title
                        }
                    }
                }
            ]
        }

        self.service.spreadsheets().batchUpdate(
            spreadsheetId=spreadsheet_id,
            body=body).execute()

    def _append_to_sheet(self, spreadsheet_id: str, sheet_title: str, rows: list[list[str]]) -> None:
        body = {
            'values': rows
        }
        self.service.spreadsheets().values().append(
            spreadsheetId=spreadsheet_id,
            range=f'{sheet_title}!A1',
            valueInputOption='USER_ENTERED',
            body=body,
            insertDataOption='INSERT_ROWS').execute()

    def _update_sheet(self, spreadsheet_id: str, sheet_title: str, rows: list[list[str]]) -> None:
        body = {
            'values': rows
        }
        self.service.spreadsheets().values().update(
            spreadsheetId=spreadsheet_id,
            range=f'{sheet_title}!A1',
            valueInputOption='USER_ENTERED',
            body=body).execute()

    def _setup_sheet_properties(self, spreadsheet_id: str, sheet_title: str) -> None:
        logger.info("Setting column width...")
        self._set_column_width(spreadsheet_id, sheet_title, 1, 1520)

        logger.info("Setting text wrap...")
        self._set_text_wrap(spreadsheet_id, sheet_title)

    def _assemble_message_rows(self, results: list[list[dict[str, str]]]) -> list[list[str]]:
        rows = []
        # Encode data to UTF-8 and Decode Unicode Escape Sequence
        encoded_data = []
        for result in results:
            encoded_row = []
            for field in result:
                encoded_cell = field['value'].encode('utf-8').decode('unicode_escape')
                encoded_row.append(encoded_cell)
            encoded_data.append(encoded_row)
        rows += encoded_data
        return rows

    def _assemble_rows(self, results: list[list[str]]) -> list[list[str]]:
        encoded_data = []
        for result in results:
            encoded_row = [field.encode('utf-8').decode('unicode_escape') for field in result]
            encoded_data.append(encoded_row)
        return encoded_data
