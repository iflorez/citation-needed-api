""" Service for interacting with AWS. """
import json
import logging
import time
from datetime import datetime, timedelta
from typing import Any
from pathlib import Path

import boto3

logger = logging.getLogger(__name__)


class MaxLogLimitExceeded(Exception):
    """ Raised when the maximum log limit is exceeded. """


class AWSService:
    """ Service for interacting with AWS. """
    MAX_LOGS_PER_QUERY = 10000

    def __init__(self):
        self.client = boto3.client('logs')

    def get_logs(self, start_date, end_date, log_group_name, query_string, cache_filename, query_by_day=False):
        results = []
        if Path(cache_filename).exists():
            logger.info("Using cached results from %s", cache_filename)
            with open(cache_filename, 'r', encoding="utf-8") as cache:
                results = json.load(cache)
        else:
            logger.info("Cache file %s not found. Querying AWS...", cache_filename)
            if query_by_day:
                current_date = start_date
                while current_date <= end_date:
                    results += self._get_logs_for_single_day(current_date, log_group_name, query_string)
                    current_date += timedelta(days=1)
            else:
                results += self._get_logs_for_single_day(start_date, log_group_name, query_string)

            # Cache the combined results into the provided cache_file
            with open(cache_filename, 'w', encoding="utf-8") as cache:
                json.dump(results, cache)

        return results

    def _get_logs_for_single_day(self, date, log_group_name, query_string):
        start_time = date
        end_time = date + timedelta(days=1)  # start with a 24-hour window
        cache_filename = f"{start_time.strftime('%Y-%m-%d')}_{end_time.strftime('%Y-%m-%d')}_cache.json"
        results = []

        if Path(cache_filename).exists():
            logger.info("Using cached results from %s", cache_filename)
            with open(cache_filename, 'r', encoding="utf-8") as cache:
                results = json.load(cache)
        else:
            logger.info("Cache file %s not found. Querying AWS...", cache_filename)
            while start_time < end_time:  # while the full day isn't covered
                cache_filename = f"{start_time.strftime('%Y-%m-%d')}_{end_time.strftime('%Y-%m-%d')}_cache.json"
                try:
                    results += self._query_logs(start_time, end_time, log_group_name, query_string)
                    # write to cache file
                    with open(cache_filename, 'w', encoding="utf-8") as cache:
                        json.dump(results, cache)
                    start_time = end_time  # set the next start_time to the previous end_time
                except MaxLogLimitExceeded:
                    # Narrow down the time range and retry
                    end_time = start_time + (end_time - start_time) / 2
        return results

    def _query_logs(self, start_time, end_time, log_group_name, query_string):
        query_id = self._start_query(start_time, end_time, log_group_name, query_string)
        return self._get_query_results(query_id)

    def _start_query(self,
                     start_date: datetime,
                     end_date: datetime,
                     log_group_name: str,
                     query_string: str) -> str:
        start_time = int(start_date.timestamp() * 1000)  # epoch milliseconds
        end_time = int((end_date + timedelta(days=1)).timestamp() * 1000)  # epoch milliseconds

        logger.info("Starting query to AWS...")

        response = self.client.start_query(
            logGroupName=log_group_name,
            startTime=start_time,
            endTime=end_time,
            queryString=query_string,
        )
        query_id = response['queryId']
        logger.info("Query started with id: %s. Waiting for results...", query_id)
        return query_id

    def _get_query_results(self, query_id: str) -> list[dict[str, Any]]:
        while True:
            response = self.client.get_query_results(queryId=query_id)
            if response['status'] == 'Complete':
                results = response['results']
                if len(results) == self.MAX_LOGS_PER_QUERY:
                    raise MaxLogLimitExceeded("Received the maximum limit of 10000 logs. There might be more logs available.")
                return results
            logger.info("Query status: %s. Waiting 1 second...", response['status'])
            time.sleep(1)
