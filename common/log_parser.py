import csv
import json
import logging
import re
import sys
from collections import defaultdict

from pydantic import ValidationError

from app.models.logging.logging import (BaseLogMessage, GoogleSearchLogMessage,
                                        HTTPLogMessage, RequestLogMessage,
                                        ResponseLogMessage, SearchLogMessage,
                                        WikipediaSearchLogMessage)
from app.models.parameters import ChatGPTRequestInfo, ChatGPTResponse
from app.models.search.search import SearchResult
from app.utils.settings_types import SearchProviderEnum, SystemEnum
from common.models.conversation import Conversation

logger = logging.getLogger(__name__)


class LogParser:
    def __init__(self, logs: list[dict[str, str]]):
        self.logs = logs

    @classmethod
    def from_rows(cls, rows: list[dict[str, str]]):
        return cls(rows)

    @classmethod
    def from_file(cls, file_path: str):
        rows = cls._read_csv_file(file_path)
        return cls(rows)

    @classmethod
    def _read_csv_file(cls, file_path: str) -> list[dict[str, str]]:
        csv.field_size_limit(sys.maxsize)
        with open(file_path, 'r') as csvfile:
            reader = csv.DictReader(csvfile, delimiter="\t")
            rows = list(reader)
        return rows

    def parse_message(self, message: str) -> ChatGPTRequestInfo | SearchResult | ChatGPTResponse | HTTPLogMessage | GoogleSearchLogMessage | WikipediaSearchLogMessage | BaseLogMessage | None:
        message_dict = json.loads(message)
        source = message_dict.get("source")
        destination = message_dict.get("destination")
        parsed_message = None
        try:
            match source, destination:
                case (SystemEnum.ChatGPT, SystemEnum.Plugin):
                    parsed_message = RequestLogMessage.parse_obj(message_dict)
                case (SystemEnum.Search, SystemEnum.Plugin):
                    parsed_message = SearchLogMessage.parse_obj(message_dict)
                    match parsed_message.info.metadata.provider:
                        case SearchProviderEnum.Google:
                            parsed_message = GoogleSearchLogMessage.parse_obj(message_dict)
                        case SearchProviderEnum.Wikipedia:
                            parsed_message = WikipediaSearchLogMessage.parse_obj(message_dict)
                case (SystemEnum.Plugin, SystemEnum.ChatGPT):
                    parsed_message = ResponseLogMessage.parse_obj(message_dict)
                case (None, None):
                    if "http" in message_dict:
                        parsed_message = HTTPLogMessage.parse_obj(message_dict)
                    else:
                        parsed_message = BaseLogMessage.parse_obj(message_dict)
                case _:
                    raise ValueError(f"Unknown message type: {message_dict}")
        except ValidationError as e:
            logger.error(f"Failed to parse message: {message} as {e.model}. Error: {e}")
            return None
        return parsed_message

    def _parse_malformed_log_message(self, message: str, log_class: type) -> dict:
        """Attempts to parse a log message that has the classname in the message itself,
        the result of a class repr"""
        output = {}
        for field_name, field_type in log_class.__annotations__.items():
            if hasattr(field_type, '__origin__') and field_type.__origin__ is list:  # It's a list
                inner_class = field_type.__args__[0]
                pattern = f"{inner_class.__name__}((.*?))"
                matches = re.findall(pattern, message)
                output[field_name] = [self._parse_malformed_log_message(match, inner_class) for match in matches]
            else:  # It's a regular field
                pattern = f"{field_name}='(.*?)'"
                match = re.search(pattern, message)
                output[field_name] = match.group(1) if match else None
        return output

    def parse_logs(self) -> list[Conversation]:
        conversations = defaultdict(Conversation)

        for log in self.logs:
            message = log.get("@message")
            if not message:
                continue
            parsed_message = self.parse_message(message)
            match parsed_message:
                case RequestLogMessage():
                    conv = conversations[parsed_message.request_id]
                    if conversation_id := parsed_message.info.user_info.openai_conversation_id:
                        conv.conversation_id = conversation_id
                    conv.request = parsed_message.info
                case GoogleSearchLogMessage():
                    conversations[parsed_message.request_id].google_search_result = parsed_message.info
                case WikipediaSearchLogMessage():
                    conversations[parsed_message.request_id].wiki_search_result = parsed_message.info
                case ResponseLogMessage():
                    conversations[parsed_message.request_id].response = parsed_message.info
        return list(conversations.values())
