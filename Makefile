# Makefile for wikipedia_chatgpt_plugin Docker image

# Docker image and container details
IMAGE_NAME := wikipedia_chatgpt_plugin

# Function to get image id
get_image_id = $(shell docker images -q $(IMAGE_NAME))

# Function to get container id
get_container_id = $(shell docker ps -a -f "name=$(IMAGE_NAME)" -q)

# Function to get SHELL_IMAGE_NAME
get_shell_image_name = $(shell docker images -q $(SHELL_IMAGE_NAME))

# Target to build the Docker image
build:
	@echo "Building Docker image..."
	docker build -t $(IMAGE_NAME) .

# Target to run the Docker container
run: build
	@echo "Running Docker container..."
	@if [ -z "$(call get_container_id)" ]; then \
		docker run --name=$(IMAGE_NAME) --env-file ./.env -dp 8000:8000 $(IMAGE_NAME); \
	else \
		echo "Container already exists"; \
	fi

# Target to stop the Docker container
stop:
	@echo "Stopping Docker container..."
	@if [ -n "$(call get_container_id)" ]; then \
		docker stop $(call get_container_id); \
	else \
		echo "Container not running"; \
	fi

# Target to remove the Docker container and clean debug images
clean: stop
	@echo "Cleaning up..."
	@if [ -n "$(call get_container_id)" ]; then \
		docker rm $(call get_container_id); \
	fi
	@docker images -f "reference=$(IMAGE_NAME)_debug_*" -q | xargs -r docker rmi -f
	@docker images -f "reference=$(IMAGE_NAME)" -q | xargs -r docker rmi -f

# Target to debug crash
debug_crash:
	@echo "Creating debug container..."
	$(eval SHELL_IMAGE_NAME=$(IMAGE_NAME)_debug_$(shell date +%s))
	@if [ -n "$(call get_container_id)" ]; then \
		docker commit $(call get_container_id) $(SHELL_IMAGE_NAME); \
		docker run -it --entrypoint /bin/bash $(SHELL_IMAGE_NAME); \
		echo "Debug container $(SHELL_IMAGE_NAME) created and entered."; \
	else \
		echo "No running container found."; \
	fi

# Target to stop and then run the Docker container
recreate: clean build run
	@echo "Recreating Docker container..."

# Target to list all related Docker containers
list:
	@echo "Listing all related Docker containers..."
	@docker ps -a -f "name=$(IMAGE_NAME)*" --format "table {{.ID}}\t{{.Image}}\t{{.Status}}\t{{.Names}}"

# Target to display the logs of the Docker container
logs:
	@docker logs -f $(call get_container_id)

.PHONY: build run stop clean debug_crash recreate list logs
