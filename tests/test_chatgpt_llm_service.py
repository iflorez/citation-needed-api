from typing import Tuple, List
from collections import namedtuple

from unittest.mock import create_autospec
from app.services.chatgpt_llm_service import ChatGPTLLMService
from unittest.mock import Mock, patch
from app.utils.settings_types import LLMInteractionStrategy

import pytest

OpenAiMessagesList = namedtuple("OpenAiMessagesList", ["data"])
OpenAiMessage = namedtuple("OpenAiMessage", ["role", "content"])
OpenAiMessageContent = namedtuple("OpenAiMessageContent", ["type", "text"])
OpenAiMessageText = namedtuple("OpenAiMessageText", ["value"])

OpenAiUsageData = namedtuple("OpenAiUsageData", ["prompt_tokens", "completion_tokens"])
OpenAiJsonModeResponse = namedtuple("OpenAiJsonModeResponse", ["choices", "usage"])
OpenAIJsonModeChoice = namedtuple("OpenAIJsonModeChoice", ["message"])
OpenAIJsonModeMessage = namedtuple("OpenAIJsonModeMessage", ["content"])

settings_mock = Mock()
settings_mock.address = "127.0.0.1"
settings_mock.openai_api_key = "test_api_key"
settings_mock.openai_assistant_id = "test_api_key"
settings_mock.gitlab_feature_flag_api_url = "test_ff_url"
settings_mock.gitlab_feature_flag_instance_id = "test_ff_instance_id"

test_toc = [
    ("page1", [("section1", 1)]),
    ("page2", [("section2", 1)]),
    ("page3", [("section3", 1), ("section4", 1)])
]

test_sections =  [
    ("Will Durant", "section1", "Knowledge is the eye of desire and can become the pilot of the soul"),
    ("Isaac Newton", "section2", "If I have seen farther than others, it is because I have stood on the shoulders of giants."),
    ("T.S. Eliot", "section3", "To each individual the world will take on a different connotation of meaning-the important lies in the desire to search for an answer.")
]

class TestChatGPTLLService:
    def test_extract_keyword_free_form(self):
        with patch('app.services.chatgpt_llm_service.get_settings') as get_settings_mock:
            get_settings_mock.return_value = settings_mock
            with patch('app.services.chatgpt_llm_service.OpenAI') as openai_mock:
                client_mock = Mock()
                openai_mock.return_value = client_mock

                client_mock.chat.completions.create.return_value = OpenAiJsonModeResponse(choices=[
                    OpenAIJsonModeChoice(message=OpenAIJsonModeMessage(content='{"keywords": ["keyword"]}'))
                ], usage=OpenAiUsageData(prompt_tokens=500, completion_tokens=50))

                chatgpt_llm_service = ChatGPTLLMService(interaction_strategy=LLMInteractionStrategy.FreeForm)
                chatgpt_llm_service.set_search_language("en")
                result = chatgpt_llm_service.extract_keyword(selection="test query")
                assert result.keywords == ["keyword"]
                assert result.error == None
                assert result.unparsed_return_value == '{"keywords": ["keyword"]}'
                assert result.tokens_in == 500
                assert result.tokens_out == 50

    def test_extract_keyword_json_mode(self):
        with patch('app.services.chatgpt_llm_service.get_settings') as get_settings_mock:
            get_settings_mock.return_value = settings_mock
            with patch('app.services.chatgpt_llm_service.OpenAI') as openai_mock:
                client_mock = Mock()
                openai_mock.return_value = client_mock
                client_mock.chat.completions.create.return_value = OpenAiJsonModeResponse(choices=[
                    OpenAIJsonModeChoice(message=OpenAIJsonModeMessage(content='{"keywords": ["keyword"]}'))
                ], usage=OpenAiUsageData(prompt_tokens=500, completion_tokens=50))

                chatgpt_llm_service = ChatGPTLLMService(interaction_strategy=LLMInteractionStrategy.JSONMode)
                chatgpt_llm_service.set_search_language("en")
                result = chatgpt_llm_service.extract_keyword(selection="test query")
                assert result.keywords == ["keyword"]
                assert result.error == None
                assert result.unparsed_return_value == '{"keywords": ["keyword"]}'
                assert result.tokens_in == 500
                assert result.tokens_out == 50

    def test_select_sections_free_form_correct_format(self):
        with patch('app.services.chatgpt_llm_service.get_settings') as get_settings_mock:
            get_settings_mock.return_value = settings_mock
            with patch('app.services.chatgpt_llm_service.OpenAI') as openai_mock:
                client_mock = Mock()
                openai_mock.return_value = client_mock

                client_mock.chat.completions.create.return_value = OpenAiJsonModeResponse(choices=[
                    OpenAIJsonModeChoice(message=OpenAIJsonModeMessage(
                        content='{"page1": ["section1"],"page2": ["section2"],"page3": ["section3"]}'
                    ))
                ], usage=OpenAiUsageData(prompt_tokens=500, completion_tokens=50))
                
                chatgpt_llm_service = ChatGPTLLMService(interaction_strategy=LLMInteractionStrategy.FreeForm)
                chatgpt_llm_service.set_search_language("en")
                result = chatgpt_llm_service.select_sections(selection="test query", tocs=test_toc)
                print(result)
                assert str(result.sections) == str([("page1", "section1"), ("page2", "section2"), ("page3", "section3")])
                assert result.error == None
                assert result.unparsed_return_value == '{"page1": ["section1"],"page2": ["section2"],"page3": ["section3"]}'
                assert result.tokens_in == 500
                assert result.tokens_out == 50

    def test_select_sections_free_form_fixable_format(self):
        """ Test that the select_sections method can handle a fixable format. We'll return one
            section with no page, one section with an incorrect page, and one section with a page
            not in the toc
        """
        with patch('app.services.chatgpt_llm_service.get_settings') as get_settings_mock:
            get_settings_mock.return_value = settings_mock
            with patch('app.services.chatgpt_llm_service.OpenAI') as openai_mock:
                client_mock = Mock()
                openai_mock.return_value = client_mock

                client_mock.chat.completions.create.return_value = OpenAiJsonModeResponse(choices=[
                    OpenAIJsonModeChoice(message=OpenAIJsonModeMessage(
                        content='{"page1": ["section1"],"page2": ["section3"],"page3": ["section2"]}'
                        ))
                ], usage=OpenAiUsageData(prompt_tokens=500, completion_tokens=50))

                chatgpt_llm_service = ChatGPTLLMService(interaction_strategy=LLMInteractionStrategy.FreeForm)
                chatgpt_llm_service.set_search_language("en")
                result = chatgpt_llm_service.select_sections(selection="test query", tocs=test_toc)
                assert str(result.sections) == str([("page3", "section3"), ("page2", "section2"), ("page1", "section1")])
                assert result.error == None
                assert result.unparsed_return_value == '{"page1": ["section1"],"page2": ["section3"],"page3": ["section2"]}'
                assert result.tokens_in == 500
                assert result.tokens_out == 50

    def test_select_sections_json_mode(self):
        with patch('app.services.chatgpt_llm_service.get_settings') as get_settings_mock:
            get_settings_mock.return_value = settings_mock
            with patch('app.services.chatgpt_llm_service.OpenAI') as openai_mock:
                client_mock = Mock()
                openai_mock.return_value = client_mock

                client_mock.chat.completions.create.return_value = OpenAiJsonModeResponse(choices=[
                    OpenAIJsonModeChoice(message=OpenAIJsonModeMessage(
                        content='{"page1": ["section1"],"page2": ["section2"],"page3": ["section3"]}'
                    ))
                ], usage=OpenAiUsageData(prompt_tokens=500, completion_tokens=50))

                chatgpt_llm_service = ChatGPTLLMService(interaction_strategy=LLMInteractionStrategy.JSONMode)
                chatgpt_llm_service.set_search_language("en")
                result = chatgpt_llm_service.select_sections(selection="test query", tocs=test_toc)
                assert str(result.sections) == str([("page1", "section1"), ("page2", "section2"), ("page3", "section3")])
                assert result.error == None
                assert result.unparsed_return_value == '{"page1": ["section1"],"page2": ["section2"],"page3": ["section3"]}'
                assert result.tokens_in == 500
                assert result.tokens_out == 50

    def test_select_quote_free_form(self):
        with patch('app.services.chatgpt_llm_service.get_settings') as get_settings_mock:
            get_settings_mock.return_value = settings_mock
            with patch('app.services.chatgpt_llm_service.OpenAI') as openai_mock:
                client_mock = Mock()
                openai_mock.return_value = client_mock

                client_mock.chat.completions.create.return_value = OpenAiJsonModeResponse(choices=[
                    OpenAIJsonModeChoice(message=OpenAIJsonModeMessage(
                        content='{"selected_quote": "%s"}' % test_sections[2][2]
                    ))
                ], usage=OpenAiUsageData(prompt_tokens=500, completion_tokens=50))

                chatgpt_llm_service = ChatGPTLLMService(interaction_strategy=LLMInteractionStrategy.FreeForm)
                chatgpt_llm_service.set_search_language("en")
                result = chatgpt_llm_service.select_quote(selection="test query", sections=test_sections)
                assert result.quote == test_sections[2]
                assert result.error == None
                assert result.no_quote_selected == False
                assert result.unparsed_return_value == '{"selected_quote": "%s"}' % test_sections[2][2]
                assert result.tokens_in == 500
                assert result.tokens_out == 50

    def test_select_quote_free_form_no_quote_selected(self):
        with patch('app.services.chatgpt_llm_service.get_settings') as get_settings_mock:
            get_settings_mock.return_value = settings_mock
            with patch('app.services.chatgpt_llm_service.OpenAI') as openai_mock:
                client_mock = Mock()
                openai_mock.return_value = client_mock

                client_mock.chat.completions.create.return_value = OpenAiJsonModeResponse(choices=[
                    OpenAIJsonModeChoice(message=OpenAIJsonModeMessage(
                        content='{"no_quote_selected": true}'
                    ))
                ], usage=OpenAiUsageData(prompt_tokens=500, completion_tokens=50))
                
                chatgpt_llm_service = ChatGPTLLMService(interaction_strategy=LLMInteractionStrategy.FreeForm)
                chatgpt_llm_service.set_search_language("en")
                result = chatgpt_llm_service.select_quote(selection="test query", sections=test_sections)
                assert result.quote == None
                assert result.error == None
                assert result.no_quote_selected == True
                assert result.unparsed_return_value == '{"no_quote_selected": true}'
                assert result.tokens_in == 500
                assert result.tokens_out == 50

    def test_select_sections_json_mode(self):
        with patch('app.services.chatgpt_llm_service.get_settings') as get_settings_mock:
            get_settings_mock.return_value = settings_mock
            with patch('app.services.chatgpt_llm_service.OpenAI') as openai_mock:
                client_mock = Mock()
                openai_mock.return_value = client_mock

                client_mock.chat.completions.create.return_value = OpenAiJsonModeResponse(choices=[
                    OpenAIJsonModeChoice(message=OpenAIJsonModeMessage(
                        content='{"selected_quote": "%s"}' % test_sections[2][2]
                    ))
                ], usage=OpenAiUsageData(prompt_tokens=500, completion_tokens=50))

                chatgpt_llm_service = ChatGPTLLMService(interaction_strategy=LLMInteractionStrategy.JSONMode)
                chatgpt_llm_service.set_search_language("en")
                result = chatgpt_llm_service.select_quote(selection="test query", sections=test_sections)
                assert result.quote == test_sections[2]
                assert result.error == None
                assert result.no_quote_selected == False
                assert result.unparsed_return_value == '{"selected_quote": "%s"}' % test_sections[2][2]
                assert result.tokens_in == 500
                assert result.tokens_out == 50

    def test_select_sections_json_mode_no_quote_selected(self):
        with patch('app.services.chatgpt_llm_service.get_settings') as get_settings_mock:
            get_settings_mock.return_value = settings_mock
            with patch('app.services.chatgpt_llm_service.OpenAI') as openai_mock:
                client_mock = Mock()
                openai_mock.return_value = client_mock

                client_mock.chat.completions.create.return_value = OpenAiJsonModeResponse(choices=[
                    OpenAIJsonModeChoice(message=OpenAIJsonModeMessage(
                        content='{"no_quote_selected": true}'
                    ))
                ], usage=OpenAiUsageData(prompt_tokens=500, completion_tokens=50))

                chatgpt_llm_service = ChatGPTLLMService(interaction_strategy=LLMInteractionStrategy.JSONMode)
                chatgpt_llm_service.set_search_language("en")
                result = chatgpt_llm_service.select_quote(selection="test query", sections=test_sections)
                assert result.quote == None
                assert result.error == None
                assert result.no_quote_selected == True
                assert result.unparsed_return_value == '{"no_quote_selected": true}'
                assert result.tokens_in == 500
                assert result.tokens_out == 50
