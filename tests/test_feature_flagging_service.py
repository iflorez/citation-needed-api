import pytest
from unittest.mock import patch, Mock
from app.services.feature_flagging_service import UnleashFeatureFlaggingService


@patch('UnleashClient.UnleashClient.initialize_client')
def test_init_failure(mock_initialize_client):
    mock_initialize_client.side_effect = Exception("Failed to initialize")
    with pytest.raises(Exception):
        UnleashFeatureFlaggingService("https://example.com", "123")


@patch('UnleashClient.UnleashClient.initialize_client')
def test_is_enabled_with_fallback_on_failure(mock_unleash_client):
    mock_unleash_client.return_value.is_enabled.side_effect = Exception("Service unavailable")

    service = UnleashFeatureFlaggingService("https://example.com", "123")
    fallback_function = Mock(return_value=False)

    assert service._is_enabled("test_feature", {"userId": "1"}, fallback_function) is False
    fallback_function.assert_called_once_with(
        "test_feature",
        {
            'appName': 'production',
            'environment': 'default',
            'userId': '1'
        }
    )
