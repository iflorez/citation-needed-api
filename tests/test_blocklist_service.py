from unittest.mock import Mock

import pytest
from fastapi import HTTPException

from app.services.blocklist_service import BlocklistService


class TestCheckBlocklistForUser:

    USER_IP_BLOCKED = '192.168.1.1'
    USER_IP_NOT_BLOCKED = '192.168.1.2'

    def setup_method(self):
        self.feature_flagging_service_mock = Mock()
        self.blocklist_service = BlocklistService(feature_flagging_service=self.feature_flagging_service_mock)

    def test_user_is_blocked(self):
        self.feature_flagging_service_mock.user_is_on_blocklist.return_value = True

        with pytest.raises(HTTPException) as excinfo:
            self.blocklist_service.check_blocklist_for_user(self.USER_IP_BLOCKED)
        assert excinfo.value.status_code == 429
        assert excinfo.value.detail == "User blocked"

    def test_user_is_not_blocked(self):
        self.feature_flagging_service_mock.user_is_on_blocklist.return_value = False

        # If user is not blocked, the method should not raise an exception
        assert self.blocklist_service.check_blocklist_for_user(self.USER_IP_NOT_BLOCKED) is None

    def test_user_is_none(self):
        self.feature_flagging_service_mock.user_is_on_blocklist.return_value = False

        # If user is None, the method should not raise an exception
        assert self.blocklist_service.check_blocklist_for_user(None) is None
